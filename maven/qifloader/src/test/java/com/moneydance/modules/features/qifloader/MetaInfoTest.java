package com.moneydance.modules.features.qifloader;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Assert;
import org.junit.Test;

import com.infinitekind.util.StreamTable;
import com.infinitekind.util.StringEncodingException;

public class MetaInfoTest {
    private static final String KEY_MODULE_NAME = "module_name";
    private static final String META_INFO_DICT = "meta_info.dict";

    @Test
    public void testMetaInfoDict() throws IOException, StringEncodingException {
        String expectedModuleName = "Load QIF Transactions";
        Class<? extends Object> clz = this.getClass();
        MetaInfoTest.testModuleName(clz, expectedModuleName);
    }

    private static final <T> void testModuleName(Class<T> clz, String expectedModuleName)
            throws StringEncodingException, IOException {
        String name = META_INFO_DICT;
        try (InputStream stream = clz.getResourceAsStream(name)) {
            Assert.assertNotNull(stream);
            StreamTable streamTable = new StreamTable();
            streamTable.readFrom(stream);
            String moduleName = streamTable.getStr(KEY_MODULE_NAME, null);
            Assert.assertNotNull(moduleName);
            Assert.assertEquals(expectedModuleName, moduleName);
        }
    }
}
