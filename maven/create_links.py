#!/usr/bin/python

import os

# cd src/main
# ln -s ../../../../src java
# ln -s ../../../../src resources
# lrwxrwxrwx. 1 root   root        15 Apr 30 03:00 python -> /usr/bin/python

# src/main/java/com/moneydance/modules/features/budgetgen/
# ../src/com/moneydance/modules/features/budgetgen/

def create_link(top_dir, dir):
  path=os.path.join(top_dir, dir, 'src', 'main', 'java', 
    'com', 'moneydance', 'modules', 'features')

  if not os.path.exists(path):
    os.makedirs(path)

  os.chdir(path)
  print('in dir=', os.path.abspath(os.getcwd()))

  path=os.path.join(os.path.dirname(top_dir), 'src',
    'com', 'moneydance', 'modules', 'features', dir)
  path=os.path.abspath(path)
  src = path
  dst = dir
  relpath = os.path.relpath(src, ".")
  if not os.path.islink(dst):
    print('Creating link dst=', dst)
    #os.symlink(src, dst)
    os.symlink(relpath, dst)

  path=os.path.join(top_dir, dir, 'src', 'main', 'resources', 
    'com', 'moneydance', 'modules', 'features')

  if not os.path.exists(path):
    os.makedirs(path)

  os.chdir(path)
  print('in dir=', os.path.abspath(os.getcwd()))

  path=os.path.join(os.path.dirname(top_dir), 'src',
    'com', 'moneydance', 'modules', 'features', dir)
  path=os.path.abspath(path)
  src = path
  dst = dir
  relpath = os.path.relpath(src, ".")
  #print('src=', src)
  #print('dst=', dst)
  #print('relpath=', relpath)
  if not os.path.islink(dst):
    print('Creating link dst=', dst)
    # ../../../../../../../../../src/com/moneydance/modules/features/
    os.symlink(relpath, dst)

dirs=[
'budgetgen',
'budgetreport',
'databeans',
'dataextractor',
'extension',
'extinstaller',
'filedisplay',
'forecaster',
'jasperreports',
'loadsectrans',
'mrbtest',
'mrbutil',
'qifloader',
'securityhistoryload',
'securitypriceload',
'securityquoteload',
]

cwd=os.getcwd()
top_dir=os.path.abspath(cwd)

for dir in dirs:
  print(dir)
  create_link(top_dir, dir)

