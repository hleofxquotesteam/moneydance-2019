/*
 * Copyright (c) 2020, Michael Bray.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
package com.moneydance.modules.features.reportwriter.view;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import com.infinitekind.util.DateUtil;
import com.moneydance.modules.features.mrbutil.MRBFXSelectionPanel;
import com.moneydance.modules.features.mrbutil.MRBFXSelectionRow;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.Main;
import com.moneydance.modules.features.reportwriter.MyGridPane;
import com.moneydance.modules.features.reportwriter.OptionMessage;
import com.moneydance.modules.features.reportwriter.Parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
public class DataDataPane {
	private Parameters params;
	private TextField name;
	private SelectionDataRow selRow;
	private SortedMap<String,DataParameter> parameters;
	private Stage stage;
	private Scene scene;
	private GridPane pane;
	private DataDataRow row;
	private GridPane parmPanes;
	private HBox buttons;
	private boolean newRow = false;
	private DatePicker fromDate;
	private DatePicker toDate;
	private CheckBox selectAccounts;
	private CheckBox selectCategories;
	private CheckBox selectBudgets;
	private CheckBox selectCurrencies;
	private CheckBox selectSecurities;
	private CheckBox selectTrans;
	private CheckBox selectInvestTrans;
	private CheckBox selAsset;
	private CheckBox selBank;
	private CheckBox selCredit;
	private CheckBox selInvestment;
	private CheckBox selLiability;
	private CheckBox selLoan;
	private Button acctSelBtn;
	private CheckBox selIncome;
	private CheckBox selExpense;
	private Button catSelBtn;
	private Button budgets;
	private Button currSelBtn;
	private Button secSelBtn;
	private CheckBox selCleared;
	private CheckBox selReconciling;
	private CheckBox selUnreconciled;
	private Button tagsSelBtn;
	private TextField fromCheque;
	private TextField toCheque;
	private Button ttypSelBtn;
	private Button invAcctBtn;
	private MRBFXSelectionPanel acctPanel=null;
	private MRBFXSelectionPanel catPanel=null;
	private MRBFXSelectionPanel budPanel=null;
	private MRBFXSelectionPanel currPanel=null;
	private MRBFXSelectionPanel secPanel=null;
	private MRBFXSelectionPanel invPanel=null;
	private MRBFXSelectionPanel ttypPanel=null;
	private MRBFXSelectionPanel tagsPanel=null;
	


	public DataDataPane(Parameters paramsp) {
		params = paramsp;
		row = new DataDataRow();
		parameters = new TreeMap<String,DataParameter>();
		row.setParameters(parameters);
		newRow= true;
		selRow = new SelectionDataRow();
	}
	public DataDataPane(Parameters paramsp, DataDataRow rowp) {
		row = rowp;
		params = paramsp;
		selRow = new SelectionDataRow();
		selRow.loadRow(row.getName(), params);
		parameters = row.getParameters();
	}
	public DataDataRow displayPanel() {
		stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		pane = new MyGridPane(Constants.WINDATADATA);
		scene = new Scene(pane);
		stage.setScene(scene);

		int ix = 0;
		int iy=0;
		parmPanes = setParameters();
		pane.add(parmPanes, ix, iy++);
		GridPane.setMargin(parmPanes, new Insets(10,10,10,10));
		GridPane.setColumnSpan(parmPanes, 3);
		ix = 0;
		buttons = new HBox();
		Button okBtn = new Button();
		if (Main.loadedIcons.okImg == null)
			okBtn.setText("OK");
		else
			okBtn.setGraphic(new ImageView(Main.loadedIcons.okImg));
		okBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (name.getText().isEmpty()) {
					OptionMessage.displayMessage("Name must be entered");
					return;
				}
				saveRow(name.getText());
				stage.close();
			}
		});
		Button cancelBtn = new Button();
		if (Main.loadedIcons.cancelImg == null)
			cancelBtn.setText("Cancel");
		else
			cancelBtn.setGraphic(new ImageView(Main.loadedIcons.cancelImg));
		cancelBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				row = null;
				stage.close();
			}
		});
		buttons.getChildren().addAll(okBtn,cancelBtn);
		HBox.setMargin(okBtn, new Insets(10,10,10,10));
		HBox.setMargin(cancelBtn, new Insets(10,10,10,10));
		pane.add(buttons, 0, iy);
		GridPane.setColumnSpan(buttons,2);
		stage.showAndWait();
		return row;
	}
	private void saveRow(String fileName) {
		boolean createRow;
		DataDataRow tempRow = new DataDataRow();
		if (!newRow && !row.getName().equals(name.getText()))
			newRow=true;
		if (newRow && tempRow.loadRow(fileName, params)) {
			if (OptionMessage.yesnoMessage("Data Parameters already exists.  Do you wish to overwrite them?")) {
				createRow = true;
				/* 
				 * new row exists and user wishes to overwrite
				 */
			}
			else
				createRow= false;
			/*
			 * new row exists and user does not wish to overwrite 
			 */
		}
		else {
			createRow = true;
			/*
			 * edit mode or new row does not exist
			 */
		}
		if (createRow) {	
			if (row==null)
				row = new DataDataRow();
			row.setName(fileName);
			updateParms();
			row.saveRow(params);
		}	
	}

	private GridPane setParameters() {
		GridPane pane = new GridPane();
		name = new TextField();
		if (!newRow) {
			name.setText(row.getName());
		}
		Label nameLbl = new Label("Name");
		Label fromDateLbl = new Label("From Date");
		fromDate = new DatePicker();
		fromDate.setConverter(Main.dateConverter); 
	    fromDate.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
	            if (!newValue){
	                fromDate.setValue(fromDate.getConverter().fromString(fromDate.getEditor().getText()));
	            }
				
			}
	    });
	    if (parameters.containsKey(Constants.PARMFROMDATE)) {
			Date tempDate = DateUtil.convertIntDateToLong(Integer.valueOf(parameters.get(Constants.PARMFROMDATE).getValue()));
			fromDate.setValue(tempDate.toInstant().atZone(Main.zone).toLocalDate());
		}
		else
			fromDate.setValue(LocalDate.now());
		Label toDateLbl = new Label("To Date");
		toDate = new DatePicker();
		toDate.setConverter(Main.dateConverter);
	    toDate.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
	            if (!newValue){
	                toDate.setValue(toDate.getConverter().fromString(toDate.getEditor().getText()));
	            }
				
			}
	    });
	    if (parameters.containsKey(Constants.PARMTODATE)) {
			Date tempDate = DateUtil.convertIntDateToLong(Integer.valueOf(parameters.get(Constants.PARMTODATE).getValue()));
			toDate.setValue(tempDate.toInstant().atZone(Main.zone).toLocalDate());
		}
		else
			toDate.setValue(LocalDate.now());
		selectAccounts = new CheckBox("Filter by Accounts");
		selectAccounts.setSelected(parameters.containsKey(Constants.PARMSELACCT)?true:false);
		selectCategories = new CheckBox("Filter by Categories");
		selectCategories.setSelected(parameters.containsKey(Constants.PARMSELCAT)?true:false);
		selectBudgets = new CheckBox("Filter by Budgets");
		selectBudgets.setSelected(parameters.containsKey(Constants.PARMSELBUDGET)?true:false);
		selectCurrencies = new CheckBox("Filter by Currencies");
		selectCurrencies.setSelected(parameters.containsKey(Constants.PARMSELCURRENCY)?true:false);
		selectSecurities = new CheckBox("Filter by Securities");
		selectSecurities.setSelected(parameters.containsKey(Constants.PARMSELSECURITY)?true:false);
		selectTrans = new CheckBox ("Filter by Transactions");
		selectTrans.setSelected(parameters.containsKey(Constants.PARMSELTRANS)?true:false);
		selectInvestTrans = new CheckBox ("Filter by Invest. Transactions");
		selectInvestTrans.setSelected(parameters.containsKey(Constants.PARMSELINVTRANS)?true:false);
		selAsset = new CheckBox("Assets");
		selAsset.setSelected(parameters.containsKey(Constants.PARMASSET)?true:false);
		selBank = new CheckBox("Banks");
		selBank.setSelected(parameters.containsKey(Constants.PARMBANK)?true:false);
		selCredit = new CheckBox("Credit Cards");
		selCredit.setSelected(parameters.containsKey(Constants.PARMCREDIT)?true:false);
		selInvestment = new CheckBox("Investments");
		selInvestment.setSelected(parameters.containsKey(Constants.PARMINVESTMENT)?true:false);
		selLiability = new CheckBox("Liabilities");
		selLiability.setSelected(parameters.containsKey(Constants.PARMLIABILITY)?true:false);
		selLoan = new CheckBox("Loans");
		selLoan.setSelected(parameters.containsKey(Constants.PARMLOAN)?true:false);
		acctSelBtn = new Button("Select");
		acctSelBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				List<MRBFXSelectionRow> list = new ArrayList<MRBFXSelectionRow>();
				List<String> selected;
				SortedMap<String,String> selMap = new TreeMap<String, String>();
				if (parameters.containsKey(Constants.PARMACCOUNTS))
					selected = parameters.get(Constants.PARMACCOUNTS).getList();
				else
					selected = new ArrayList<>();
				if (selAsset.isSelected())
					list.addAll(Main.extension.assetAccounts);
				if (selBank.isSelected())
					list.addAll(Main.extension.bankAccounts);
				if (selCredit.isSelected())
					list.addAll(Main.extension.creditAccounts);
				if (selLiability.isSelected())
					list.addAll(Main.extension.liabilityAccounts);
				if (selInvestment.isSelected())
					list.addAll(Main.extension.investmentAccounts);
				if (selLoan.isSelected())
					list.addAll(Main.extension.loanAccounts);
				if (list.isEmpty()) {
					OptionMessage.displayMessage("Please select at least one type of account");
					return;
				}
				for (String acct : selected) {
					selMap.put(acct,acct);
				}
				for (MRBFXSelectionRow row : list) {
					if (selMap.get(row.getRowId()) != null)
						row.setSelected(true);;
				}
				acctPanel = new MRBFXSelectionPanel(list);
				acctPanel.display();
			}
		});
		selIncome = new CheckBox("Income");
		selIncome.setSelected(parameters.containsKey(Constants.PARMINCOME)?true:false);
		selExpense = new CheckBox("Expense");
		selExpense.setSelected(parameters.containsKey(Constants.PARMEXPENSE)?true:false);
		catSelBtn = new Button("Select");
		catSelBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				List<MRBFXSelectionRow> list = new ArrayList<MRBFXSelectionRow>();
				List<String> selected;
				SortedMap<String,String> selMap = new TreeMap<String, String>();
				if (parameters.containsKey(Constants.PARMCATEGORIES))
					selected = parameters.get(Constants.PARMCATEGORIES).getList();
				else
					selected = new ArrayList<>();
				if (selExpense.isSelected())
					list.addAll(Main.extension.expenseCategories);
				if (selIncome.isSelected())
					list.addAll(Main.extension.incomeCategories);
				if (list.isEmpty()) {
					OptionMessage.displayMessage("Please select at least one type of category");
					return;
				}
				for (String acct : selected) {
					selMap.put(acct,acct);
				}
				for (MRBFXSelectionRow row : list) {
					if (selMap.get(row.getRowId()) != null)
						row.setSelected(true);;
				}
				catPanel = new MRBFXSelectionPanel(list);
				catPanel.display();
			}
		});
		budgets = new Button("Select");
		budgets.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				List<MRBFXSelectionRow> list = new ArrayList<MRBFXSelectionRow>(Main.extension.budgets);
				List<String> selected;
				SortedMap<String,String> selMap = new TreeMap<String, String>();
				if (parameters.containsKey(Constants.PARMBUDGET))
					selected = parameters.get(Constants.PARMBUDGET).getList();
				else
					selected = new ArrayList<>();
				for (String acct : selected) {
					selMap.put(acct,acct);
				}
				for (MRBFXSelectionRow row : list) {
					if (selMap.get(row.getRowId()) != null)
						row.setSelected(true);;
				}
				budPanel = new MRBFXSelectionPanel(list);
				budPanel.display();
			}
		});		
		currSelBtn = new Button("Select");
		currSelBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				List<MRBFXSelectionRow> list = new ArrayList<MRBFXSelectionRow>(Main.extension.currencies);
				List<String> selected;
				SortedMap<String,String> selMap = new TreeMap<String, String>();
				if (parameters.containsKey(Constants.PARMCURRENCY))
					selected = parameters.get(Constants.PARMCURRENCY).getList();
				else
					selected = new ArrayList<>();
				for (String acct : selected) {
					selMap.put(acct,acct);
				}
				for (MRBFXSelectionRow row : list) {
					if (selMap.get(row.getRowId()) != null)
						row.setSelected(true);;
				}
				currPanel = new MRBFXSelectionPanel(list);
				currPanel.display();
			}
		});		
		secSelBtn = new Button("Select");
		secSelBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				List<MRBFXSelectionRow> list = new ArrayList<MRBFXSelectionRow>(Main.extension.securities);
				List<String> selected;
				SortedMap<String,String> selMap = new TreeMap<String, String>();
				if (parameters.containsKey(Constants.PARMSECURITY))
					selected = parameters.get(Constants.PARMSECURITY).getList();
				else
					selected = new ArrayList<>();
				for (String acct : selected) {
					selMap.put(acct,acct);
				}
				for (MRBFXSelectionRow row : list) {
					if (selMap.get(row.getRowId()) != null)
						row.setSelected(true);;
				}
				secPanel = new MRBFXSelectionPanel(list);
				secPanel.display();
			}
		});		
		selCleared = new CheckBox("Cleared");
		selCleared.setSelected(parameters.containsKey(Constants.PARMCLEARED)?true:false);
		selReconciling = new CheckBox("Reconciling");
		selReconciling.setSelected(parameters.containsKey(Constants.PARMRECON)?true:false);
		selUnreconciled = new CheckBox("Unreconciled");
		selUnreconciled.setSelected(parameters.containsKey(Constants.PARMUNRECON)?true:false);
		tagsSelBtn = new Button("Filter by Tags");
		tagsSelBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				List<MRBFXSelectionRow> list = new ArrayList<MRBFXSelectionRow>(Main.extension.tags);
				List<String> selected;
				SortedMap<String,String> selMap = new TreeMap<String, String>();
				if (parameters.containsKey(Constants.PARMTAGS))
					selected = parameters.get(Constants.PARMTAGS).getList();
				else
					selected = new ArrayList<>();
				for (String acct : selected) {
					selMap.put(acct,acct);
				}
				for (MRBFXSelectionRow row : list) {
					if (selMap.get(row.getRowId()) != null)
						row.setSelected(true);;
				}
				tagsPanel = new MRBFXSelectionPanel(list);
				tagsPanel.display();
			}
		});				Label fromChequeLbl = new Label("From Cheque No");
		fromCheque = new TextField();
		if (parameters.containsKey(Constants.PARMFROMCHEQUE))
			fromCheque.setText(parameters.get(Constants.PARMFROMCHEQUE).getValue());
		Label toChequeLbl = new Label("To Cheque No");
		toCheque = new TextField();
		if (parameters.containsKey(Constants.PARMTOCHEQUE))
			toCheque.setText(parameters.get(Constants.PARMTOCHEQUE).getValue());
		invAcctBtn = new Button("Select Accounts and Securities");
		invAcctBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				List<MRBFXSelectionRow> list = new ArrayList<MRBFXSelectionRow>(Main.extension.securityAccounts);
				List<String> selected;
				SortedMap<String,String> selMap = new TreeMap<String, String>();
				if (parameters.containsKey(Constants.PARMINVACCTS))
					selected = parameters.get(Constants.PARMINVACCTS).getList();
				else
					selected = new ArrayList<>();
				for (String acct : selected) {
					selMap.put(acct,acct);
				}
				for (MRBFXSelectionRow row : list) {
					if (selMap.get(row.getRowId()) != null)
						row.setSelected(true);;
				}
				invPanel = new MRBFXSelectionPanel(list);
				invPanel.display();
			}
		});		
		ttypSelBtn = new Button("Filter by Transfer Types");
		ttypSelBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				List<MRBFXSelectionRow> list = new ArrayList<MRBFXSelectionRow>(Main.extension.transferTypes);
				List<String> selected;
				SortedMap<String,String> selMap = new TreeMap<String, String>();
				if (parameters.containsKey(Constants.PARMTRANSFER))
					selected = parameters.get(Constants.PARMTRANSFER).getList();
				else
					selected = new ArrayList<>();
				for (String acct : selected) {
					selMap.put(acct,acct);
				}
				for (MRBFXSelectionRow row : list) {
					if (selMap.get(row.getRowId()) != null)
						row.setSelected(true);;
				}
				ttypPanel = new MRBFXSelectionPanel(list);
				ttypPanel.display();
			}
		});		

		int ix = 0;
		int iy = 0;
		pane.add(nameLbl, ix++,iy);
		pane.add(name, ix, iy++);
		ix= 1;
		HBox datePane = new HBox();
		datePane.setSpacing(5.0);
		datePane.getChildren().addAll(fromDateLbl,fromDate,toDateLbl,toDate);
		pane.add(datePane, ix, iy++);
		GridPane.setColumnSpan(datePane, 4);
		ix=0;
		pane.add(selectAccounts, ix++, iy);
		pane.add(selAsset, ix++, iy);
		pane.add(selBank, ix++, iy);
		pane.add(selCredit, ix++, iy);
		pane.add(acctSelBtn, ix, iy++);
		ix=1;
		pane.add(selInvestment,ix++,iy);
		pane.add(selLiability, ix++, iy);
		pane.add(selLoan, ix, iy++);
		ix = 0;
		pane.add(selectCategories,ix++,iy);
		pane.add(selIncome, ix++, iy);
		pane.add(selExpense, ix++, iy);
		pane.add(catSelBtn, ix, iy++);
		ix=0;
		pane.add(selectBudgets, ix++, iy);
		pane.add(budgets, ix, iy++);
		ix=0;
		pane.add(selectCurrencies, ix++, iy);
		pane.add(currSelBtn, ix, iy++);
		ix=0;
		pane.add(selectSecurities, ix++, iy);
		pane.add(secSelBtn, ix, iy++);
		ix=0;
		pane.add(selectTrans, ix++, iy);
		pane.add(selCleared, ix++, iy);
		pane.add(selUnreconciled, ix++, iy);
		pane.add(selReconciling, ix, iy++);
		ix=1;
		pane.add(fromChequeLbl, ix++, iy);
		pane.add(fromCheque, ix, iy++);
		ix=1;
		pane.add(toChequeLbl, ix++, iy);
		pane.add(toCheque, ix, iy++);
		ix=1;
		pane.add(tagsSelBtn, ix, iy++);
		ix=0;
		pane.add(selectInvestTrans, ix++, iy);
		pane.add(invAcctBtn, ix++, iy);
		pane.add(ttypSelBtn, ix, iy++);
		ix=0;
//		pane.add(selParent, ix++, iy);
//		pane.add(selOther, ix++, iy);
		pane.setHgap(10.0);
		pane.setVgap(10.0);
		return pane;
	}
	private void updateParms() {
		DataParameter nullParm = new DataParameter();
		nullParm.setValue(null);
		DataParameter fromDateParm = new DataParameter();
		Date tempDate =Date.from(fromDate.getValue().atStartOfDay().atZone(Main.zone).toInstant());
		int intDate=DateUtil.convertDateToInt(tempDate);
		fromDateParm.setValue(String.valueOf(intDate));
		parameters.put(Constants.PARMFROMDATE, fromDateParm);
		DataParameter toDateParm = new DataParameter();
		tempDate =Date.from(toDate.getValue().atStartOfDay().atZone(Main.zone).toInstant());
		intDate=DateUtil.convertDateToInt(tempDate);
		toDateParm.setValue(String.valueOf(intDate));
		parameters.put(Constants.PARMTODATE, toDateParm);
		if (selectAccounts.isSelected())
			parameters.put(Constants.PARMSELACCT,nullParm);
		else
			parameters.remove(Constants.PARMSELACCT);
		if (selectCategories.isSelected())
			parameters.put(Constants.PARMSELCAT,nullParm);
		else
			parameters.remove(Constants.PARMSELCAT);
		if (selectBudgets.isSelected())
			parameters.put(Constants.PARMSELBUDGET,nullParm);
		else
			parameters.remove(Constants.PARMSELBUDGET);
		if (selectCurrencies.isSelected())
			parameters.put(Constants.PARMSELCURRENCY,nullParm);
		else
			parameters.remove(Constants.PARMSELCURRENCY);
		if (selectSecurities.isSelected())
			parameters.put(Constants.PARMSELSECURITY,nullParm);
		else
			parameters.remove(Constants.PARMSELSECURITY);
		if (selectTrans.isSelected())
			parameters.put(Constants.PARMSELTRANS,nullParm);
		else
			parameters.remove(Constants.PARMSELTRANS);
		if (selAsset.isSelected())
			parameters.put(Constants.PARMASSET,nullParm);
		else
			parameters.remove(Constants.PARMASSET);
		if (selBank.isSelected())
			parameters.put(Constants.PARMBANK,nullParm);
		else
			parameters.remove(Constants.PARMBANK);
		if (selCredit.isSelected())
			parameters.put(Constants.PARMCREDIT,nullParm);
		else
			parameters.remove(Constants.PARMCREDIT);
		if (selLiability.isSelected())
			parameters.put(Constants.PARMLIABILITY,nullParm);
		else
			parameters.remove(Constants.PARMLIABILITY);
		if (selLoan.isSelected())
			parameters.put(Constants.PARMLOAN,nullParm);
		else
			parameters.remove(Constants.PARMLOAN);
		if (selInvestment.isSelected())
			parameters.put(Constants.PARMINVESTMENT,nullParm);
		else
			parameters.remove(Constants.PARMINVESTMENT);
		if (selIncome.isSelected())
			parameters.put(Constants.PARMINCOME,nullParm);
		else
			parameters.remove(Constants.PARMINCOME);
		if (selExpense.isSelected())
			parameters.put(Constants.PARMEXPENSE,nullParm);
		else
			parameters.remove(Constants.PARMEXPENSE);
		if (selCleared.isSelected())
			parameters.put(Constants.PARMCLEARED,nullParm);
		else
			parameters.remove(Constants.PARMCLEARED);
		if (selReconciling.isSelected())
			parameters.put(Constants.PARMRECON,nullParm);
		else
			parameters.remove(Constants.PARMRECON);
		if (selUnreconciled.isSelected())
			parameters.put(Constants.PARMUNRECON,nullParm);
		else
			parameters.remove(Constants.PARMUNRECON);		
		if (fromCheque.getText().isBlank())
			parameters.remove(Constants.PARMFROMCHEQUE);
		else {
			DataParameter parm = new DataParameter();
			parm.setValue(fromCheque.getText());
			parameters.put(Constants.PARMFROMCHEQUE, parm);
		}
		if (toCheque.getText().isBlank())
			parameters.remove(Constants.PARMTOCHEQUE);
		else {
			DataParameter parm = new DataParameter();
			parm.setValue(toCheque.getText());
			parameters.put(Constants.PARMTOCHEQUE, parm);
		}

		if (selectInvestTrans.isSelected())
			parameters.put(Constants.PARMSELINVTRANS,nullParm);
		else
			parameters.remove(Constants.PARMSELINVTRANS);

		if (acctPanel != null)
		{
			DataParameter parm = new DataParameter();
			parm.setList(acctPanel.getSelected());
			parameters.put(Constants.PARMACCOUNTS, parm);
		}
		if (catPanel != null)
		{
			DataParameter parm = new DataParameter();
			parm.setList(catPanel.getSelected());
			parameters.put(Constants.PARMCATEGORIES, parm);
		}
		if (budPanel != null)
		{
			DataParameter parm = new DataParameter();
			parm.setList(budPanel.getSelected());
			parameters.put(Constants.PARMBUDGET, parm);
		}
		if (currPanel != null)
		{
			DataParameter parm = new DataParameter();
			parm.setList(currPanel.getSelected());
			parameters.put(Constants.PARMCURRENCY, parm);
		}
		if (secPanel != null)
		{
			DataParameter parm = new DataParameter();
			parm.setList(secPanel.getSelected());
			parameters.put(Constants.PARMSECURITY, parm);
		}
		if (ttypPanel != null)
		{
			DataParameter parm = new DataParameter();
			parm.setList(ttypPanel.getSelected());
			parameters.put(Constants.PARMTRANSFER, parm);
		}
		if (invPanel != null  && !invPanel.getSelected().isEmpty())
		{
			DataParameter parm = new DataParameter();
			parm.setList(invPanel.getSelected());
			parameters.put(Constants.PARMINVACCTS, parm);
		}
		if (tagsPanel != null && !tagsPanel.getSelected().isEmpty())
		{
			DataParameter parm = new DataParameter();
			parm.setList(tagsPanel.getSelected());
			parameters.put(Constants.PARMTAGS, parm);
		}
	}
}

