package com.moneydance.modules.features.reportwriter.view;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.moneydance.modules.features.reportwriter.Main;
import com.moneydance.modules.features.reportwriter.MyGridPane;
import com.moneydance.modules.features.reportwriter.databeans.DataBean;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FieldSelectionPane {
		private Stage stage;
		private Scene scene;
		private MyGridPane pane;
		private HBox buttons;
		private ObservableList<FieldSelectionRow> model;
	    private TableView<FieldSelectionRow> thisTable;
	    private List<String> current;
	    private String windowName;

		
		public FieldSelectionPane(String windowName,DataBean bean,List<String> current) {
			this.current = current;
			this.windowName = windowName;
			Field[] fields = bean.getClass().getDeclaredFields();
			List<FieldSelectionRow> fieldList = new ArrayList<FieldSelectionRow>();
			for (Field field : fields) {
				if (!field.isAnnotationPresent(ColumnName.class)) // not database field
					continue;
				ColumnName name = field.getAnnotation(ColumnName.class);
				String fldTitle;
				if (field.isAnnotationPresent(ColumnTitle.class)) {
					ColumnTitle title = field.getAnnotation(ColumnTitle.class);
					fldTitle = title.value();
				}
				else
					fldTitle = name.value();
				boolean selected;
				if (current.contains(name.value()))
					selected = true;
				else
					selected = false;;
				FieldSelectionRow fldRow = new FieldSelectionRow(name.value(),fldTitle,selected);
				fieldList.add(fldRow);

			}
			model = FXCollections.observableArrayList(fieldList);

		}
		public List<String> displayPanel() {
			stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			pane = new MyGridPane(windowName);
			scene = new Scene(pane);
			stage.setScene(scene);

			int ix = 0;
			int iy=0;
			setUpTable();
			pane.add(thisTable, ix, iy++);
			GridPane.setColumnSpan(thisTable, 2);
			ix = 0;
			buttons = new HBox();
			Button okBtn = new Button();
			if (Main.loadedIcons.okImg == null)
				okBtn.setText("OK");
			else
				okBtn.setGraphic(new ImageView(Main.loadedIcons.okImg));
			okBtn.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e) {
					updateParms();
					stage.close();
				}
			});
			Button cancelBtn = new Button();
			if (Main.loadedIcons.cancelImg == null)
				cancelBtn.setText("Cancel");
			else
				cancelBtn.setGraphic(new ImageView(Main.loadedIcons.cancelImg));
			cancelBtn.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e) {
					stage.close();
				}
			});
			buttons.getChildren().addAll(okBtn,cancelBtn);
			HBox.setMargin(okBtn, new Insets(10,10,10,10));
			HBox.setMargin(cancelBtn, new Insets(10,10,10,10));
			pane.add(buttons, 0, iy);
			GridPane.setColumnSpan(buttons,2);
			stage.showAndWait();
			return current;
		}

		private void updateParms() {
			current = new ArrayList<String>();
			for (FieldSelectionRow row : model) {
				if (row.getSelected())
					current.add(row.getFieldName());
			}
		}

	@SuppressWarnings("unchecked")
	private void setUpTable () {
		thisTable = new TableView<>();
		thisTable.setEditable(true);
		thisTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);


		/*
		 * Name
		 */
		TableColumn<FieldSelectionRow,String> name = new TableColumn<>("Field Name");
		/*
		 * Last Created
		 */
		TableColumn<FieldSelectionRow,CheckBox> included = new TableColumn<>("Included");
		thisTable.getColumns().addAll(name,included);
		thisTable.setItems(model);
		name.setCellValueFactory(new PropertyValueFactory<>("fieldTitle"));
		included.setCellValueFactory(new PropertyValueFactory<>("included"));


	}


}
