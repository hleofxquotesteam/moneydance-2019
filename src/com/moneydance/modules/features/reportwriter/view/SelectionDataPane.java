package com.moneydance.modules.features.reportwriter.view;


import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.Main;
import com.moneydance.modules.features.reportwriter.MyGridPane;
import com.moneydance.modules.features.reportwriter.OptionMessage;
import com.moneydance.modules.features.reportwriter.Parameters;
import com.moneydance.modules.features.reportwriter.databeans.AccountBean;
import com.moneydance.modules.features.reportwriter.databeans.AddressBean;
import com.moneydance.modules.features.reportwriter.databeans.BudgetBean;
import com.moneydance.modules.features.reportwriter.databeans.BudgetItemBean;
import com.moneydance.modules.features.reportwriter.databeans.CategoryBean;
import com.moneydance.modules.features.reportwriter.databeans.CurrencyBean;
import com.moneydance.modules.features.reportwriter.databeans.CurrencyRateBean;
import com.moneydance.modules.features.reportwriter.databeans.DataBean;
import com.moneydance.modules.features.reportwriter.databeans.InvTranBean;
import com.moneydance.modules.features.reportwriter.databeans.ReminderBean;
import com.moneydance.modules.features.reportwriter.databeans.SecurityBean;
import com.moneydance.modules.features.reportwriter.databeans.SecurityPriceBean;
import com.moneydance.modules.features.reportwriter.databeans.TransactionBean;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SelectionDataPane  {
	private Parameters params;
	private TextField name;
	private Button accountsFieldBtn;
	private Button addressFieldBtn;
	private Button budgetsFieldBtn;
	private Button currenciesFieldBtn;
	private Button securitiesFieldBtn;
	private Button transactionsFieldBtn;
	private Button categoriesFieldBtn;
	private Button invTransFieldBtn;
	private Button budgetItemsFieldBtn;
	private Button securityPricesFieldBtn;
	private Button currencyRatesFieldBtn;
	private Button remindersFieldBtn;
	private CheckBox accountsCB;
	private CheckBox addressCB ;
	private CheckBox budgetsCB;
	private CheckBox currenciesCB;
	private CheckBox securitiesCB;
	private CheckBox transactionsCB;
	private CheckBox invTransCB;
	private CheckBox budgetItemsCB;
	private CheckBox categoriesCB;
	private CheckBox securityPricesCB;
	private CheckBox currencyRatesCB;
	private CheckBox remindersCB;
	private Stage stage;
	private Scene scene;
	private GridPane pane;
	private SelectionDataRow row;
	private boolean newRow = false;
	private SortedMap<String,DataParameter> dataParams;
	
	public SelectionDataPane(Parameters params) {
		this.params = params;
		row = new SelectionDataRow();
		dataParams = new TreeMap<String,DataParameter>();
		newRow= true;
	}
	public SelectionDataPane(Parameters params, SelectionDataRow row) {
		this.row = row;
		this.params = params;
		dataParams = row.getParameters();
		if (dataParams == null )
			dataParams = new TreeMap<String,DataParameter>();	
	}
	public SelectionDataRow displayPanel() {
		stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		pane = new MyGridPane(Constants.WINSELECTIONDATA);
		scene = new Scene(pane);
		stage.setScene(scene);
		Label selectionLbl = new Label("Selection Name");
		name = new TextField();
		GridPane.setMargin(name,new Insets(10,10,10,10));
		accountsCB = new CheckBox("Accounts");
		accountsCB.setPadding(new Insets(10,10,10,10));
		accountsFieldBtn = new Button("Fields");
		GridPane.setMargin(accountsFieldBtn,new Insets(10,10,10,10));
		accountsFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new AccountBean(),Constants.PARMFLDACCT);
			}
		});
		addressCB = new CheckBox("Addresses");
		addressCB.setPadding(new Insets(10,10,10,10));
		addressFieldBtn = new Button("Fields");
		GridPane.setMargin(addressFieldBtn,new Insets(10,10,10,10));
		addressFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new AddressBean(),Constants.PARMFLDADDR);
			}
		});
		budgetsCB = new CheckBox("Budgets");
		budgetsCB.setPadding(new Insets(10,10,10,10));
		budgetsFieldBtn = new Button("Fields");
		GridPane.setMargin(budgetsFieldBtn,new Insets(10,10,10,10));
		budgetsFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new BudgetBean(),Constants.PARMFLDBUDG);
			}
		});
		currenciesCB = new CheckBox("Currencies");
		currenciesCB.setPadding(new Insets(10,10,10,10));
		currenciesFieldBtn = new Button("Fields");
		GridPane.setMargin(currenciesFieldBtn,new Insets(10,10,10,10));
		currenciesFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new CurrencyBean(),Constants.PARMFLDCUR);
			}
		});
		securitiesCB = new CheckBox("Securities");
		securitiesCB.setPadding(new Insets(10,10,10,10));
		securitiesFieldBtn = new Button("Fields");
		GridPane.setMargin(securitiesFieldBtn,new Insets(10,10,10,10));
		securitiesFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new SecurityBean(),Constants.PARMFLDSEC);
			}
		});
		transactionsCB = new CheckBox("Transactions");
		transactionsCB.setPadding(new Insets(10,10,10,10));
		transactionsFieldBtn = new Button("Fields");
		GridPane.setMargin(transactionsFieldBtn,new Insets(10,10,10,10));
		transactionsFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new TransactionBean(),Constants.PARMFLDTRAN);
			}
		});
		invTransCB = new CheckBox("Invest. Trans");
		invTransCB.setPadding(new Insets(10,10,10,10));
		invTransFieldBtn = new Button("Fields");
		GridPane.setMargin(invTransFieldBtn,new Insets(10,10,10,10));
		invTransFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new InvTranBean(),Constants.PARMFLDINVTRAN);
			}
		});
		budgetItemsCB = new CheckBox("Budget Items");
		budgetItemsCB.setPadding(new Insets(10,10,10,10));
		budgetItemsFieldBtn = new Button("Fields");
		GridPane.setMargin(budgetItemsFieldBtn,new Insets(10,10,10,10));
		budgetItemsFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new BudgetItemBean(),Constants.PARMFLDBUDGI);
			}
		});
		categoriesCB = new CheckBox("Categories");
		categoriesCB.setPadding(new Insets(10,10,10,10));
		categoriesFieldBtn = new Button("Fields");
		GridPane.setMargin(categoriesFieldBtn,new Insets(10,10,10,10));
		categoriesFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new CategoryBean(),Constants.PARMFLDCAT);
			}
		});
		securityPricesCB = new CheckBox("Sec. prices");
		securityPricesCB.setPadding(new Insets(10,10,10,10));
		securityPricesFieldBtn = new Button("Fields");
		GridPane.setMargin(securityPricesFieldBtn,new Insets(10,10,10,10));
		securityPricesFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new SecurityPriceBean(),Constants.PARMFLDSECP);
			}
		});
		currencyRatesCB = new CheckBox("Currency Rates");
		currencyRatesCB.setPadding(new Insets(10,10,10,10));
		currencyRatesFieldBtn = new Button("Fields");
		GridPane.setMargin(currencyRatesFieldBtn,new Insets(10,10,10,10));
		currencyRatesFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new CurrencyRateBean(),Constants.PARMFLDCURR);
			}
		});
		remindersCB = new CheckBox("Reminders");
		remindersCB.setPadding(new Insets(10,10,10,10));
		remindersFieldBtn = new Button("Fields");
		GridPane.setMargin(remindersFieldBtn,new Insets(10,10,10,10));
		remindersFieldBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				displayFields(new ReminderBean(),Constants.PARMFLDREM);
			}
		});
		if (!newRow) {
			name.setText(row.getName());
			accountsCB.setSelected(row.getAccounts());
			addressCB.setSelected(row.getAddress());
			budgetsCB.setSelected(row.getBudgets());
			currenciesCB.setSelected(row.getCurrencies());
			securitiesCB.setSelected(row.getSecurities());
			transactionsCB.setSelected(row.getTransactions());
			invTransCB.setSelected(row.getInvTrans());
			budgetItemsCB.setSelected(row.getBudgetItems());
			categoriesCB.setSelected(row.getCategories());
			securityPricesCB.setSelected(row.getSecurityPrices());
			currencyRatesCB.setSelected(row.getCurrencyRates());
			remindersCB.setSelected(row.getReminders());
		}
		int ix = 0;
		int iy = 0;
		pane.add(selectionLbl, ix++, iy);
		pane.add(name, ix, iy++);
		GridPane.setColumnSpan(name, 2);
		ix = 0;
		pane.add(accountsCB, ix++, iy);
		pane.add(accountsFieldBtn, ix++, iy);
		pane.add(addressCB,ix++, iy);
		pane.add(addressFieldBtn, ix++, iy);
		pane.add(budgetsCB,ix++, iy);
		pane.add(budgetsFieldBtn, ix, iy++);
		ix = 0;
		pane.add(transactionsCB,ix++, iy);
		pane.add(transactionsFieldBtn, ix++, iy);
		pane.add(invTransCB,ix++, iy);
		pane.add(invTransFieldBtn, ix++, iy);
		pane.add(budgetItemsCB,ix++, iy);
		pane.add(budgetItemsFieldBtn, ix, iy++);
		ix=0;
		pane.add(securitiesCB, ix++, iy);
		pane.add(securitiesFieldBtn, ix++, iy);
		pane.add(currenciesCB,ix++, iy);
		pane.add(currenciesFieldBtn, ix++, iy);
		pane.add(categoriesCB,ix++, iy);
		pane.add(categoriesFieldBtn, ix, iy++);
		ix=0;
		pane.add(securityPricesCB,ix++, iy);
		pane.add(securityPricesFieldBtn, ix++, iy);
		pane.add(currencyRatesCB,ix++, iy);
		pane.add(currencyRatesFieldBtn, ix++, iy);
		pane.add(remindersCB,ix++, iy);
		pane.add(remindersFieldBtn, ix, iy++);
		ix=0;
		Button okBtn = new Button();
		GridPane.setMargin(okBtn,new Insets(10,10,10,10));
		if (Main.loadedIcons.okImg == null)
			okBtn.setText("OK");
		else
			okBtn.setGraphic(new ImageView(Main.loadedIcons.okImg));
		okBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (name.getText().isEmpty()) {
					OptionMessage.displayMessage("Name must be entered");
					return;
				}
				int count = 0;
				if (accountsCB.isSelected()) 
					count++;
				if (addressCB.isSelected())
					count++;
				if (budgetsCB.isSelected())
					count++;
				if (currenciesCB.isSelected())
					count++;
				if (securitiesCB.isSelected())
					count++;
				if (transactionsCB.isSelected())
					count++;
				if (invTransCB.isSelected())
					count++;
				if (budgetItemsCB.isSelected())
					count++;
				if (categoriesCB.isSelected())
					count++;
				if (securityPricesCB.isSelected())
					count++;
				if (currencyRatesCB.isSelected())
					count++;
				if (remindersCB.isSelected())
					count++;
				if (count < 1) {
					OptionMessage.displayMessage("At least one selection must be chosen");
					return;
				}
				else
				{
					boolean createRow;
					SelectionDataRow tempRow = new SelectionDataRow();
					if (newRow && tempRow.loadRow(name.getText(), params)) {
						if (OptionMessage.yesnoMessage("Selection Template already exists.  Do you wish to overwrite it?")) {
							createRow = true;
							/* 
							 * new row exists and user wishes to overwrite
							 */
						}
						else
							createRow= false;
						/*
						 * new row exists and user does not wish to overwrite 
						 */
					}
					else {
						createRow = true;
						/*
						 * edit mode or new row does not exist
						 */
					}
					if (createRow) {	
						row.setName(name.getText());
						row.setAccounts(accountsCB.isSelected());
						row.setAddress(addressCB.isSelected());
						row.setBudgets(budgetsCB.isSelected());
						row.setTransactions(transactionsCB.isSelected());
						row.setInvTrans(invTransCB.isSelected());
						row.setCurrencies(currenciesCB.isSelected());
						row.setSecurities(securitiesCB.isSelected());
						row.setBudgetItems(budgetItemsCB.isSelected());
						row.setCategories(categoriesCB.isSelected());
						row.setSecurityPrices(securityPricesCB.isSelected());
						row.setCurrencyRates(currencyRatesCB.isSelected());
						row.setReminders(remindersCB.isSelected());
						row.setParameters(dataParams);
						row.saveRow(params);
					}
				}
				stage.close();
			}
		});
		Button cancelBtn = new Button();
		GridPane.setMargin(cancelBtn,new Insets(10,10,10,10));
		if (Main.loadedIcons.cancelImg == null)
			cancelBtn.setText("Cancel");
		else
			cancelBtn.setGraphic(new ImageView(Main.loadedIcons.cancelImg));
		cancelBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				row=null;
				stage.close();
			}
		});
		ix=1;
		pane.add(okBtn, ix++, iy);
		pane.add(cancelBtn,ix++, iy);
		stage.showAndWait();
		return row;
	}

	private void displayFields(DataBean bean, String parm) {
		List<String> selectedFields;
		if (dataParams.containsKey(parm)) 
			selectedFields = dataParams.get(parm).getList();
		else
			selectedFields = new ArrayList<String>();
		FieldSelectionPane fieldPane = new FieldSelectionPane(parm,bean,selectedFields);
		selectedFields= fieldPane.displayPanel();
		if (selectedFields.isEmpty()) {
			if(dataParams.containsKey(parm))
				dataParams.remove(parm);
		}
		else {
			if (dataParams.containsKey(parm))
				dataParams.get(parm).setList(selectedFields);
			else {
					DataParameter newParams = new DataParameter();
					newParams.setList(selectedFields);
					dataParams.put(parm, newParams);
			}
		}
			
	}

}
