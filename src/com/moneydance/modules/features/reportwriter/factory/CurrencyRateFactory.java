package com.moneydance.modules.features.reportwriter.factory;

import java.util.List;
import java.util.SortedMap;

import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.CurrencySnapshot;
import com.infinitekind.moneydance.model.CurrencyTable;
import com.infinitekind.moneydance.model.CurrencyType;
import com.infinitekind.util.DateUtil;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.CurrencyRateBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.DataParameter;

public class CurrencyRateFactory {
	private DataDataRow dataParams;
	private CurrencyTable curTable;
	private List<CurrencyType> selectedCurrencies;
	private List<String> currencies=null;
	private SortedMap<String, DataParameter> map;
	private AccountBook book;
	private int fromDate;
	private int toDate;

public CurrencyRateFactory(AccountBook bookp, DataDataRow dataParamsp,OutputFactory output) throws RWException {
	book = bookp;
	dataParams = dataParamsp;
	map = dataParams.getParameters();
	if (map.containsKey(Constants.PARMCURRENCY)) 
		currencies = map.get(Constants.PARMCURRENCY).getList();
	if (map.containsKey(Constants.PARMFROMDATE))
		fromDate = Integer.valueOf(map.get(Constants.PARMFROMDATE).getValue());
	else 
		fromDate = DateUtil.getStrippedDateInt();
	if (map.containsKey(Constants.PARMTODATE))
		toDate = Integer.valueOf(map.get(Constants.PARMTODATE).getValue());
	else 
		toDate = DateUtil.getStrippedDateInt();	curTable = book.getCurrencies();
	curTable = book.getCurrencies();
	if (curTable == null)
		return;
	selectedCurrencies= curTable.getAllCurrencies();
	if (selectedCurrencies == null)
		return;
	for (CurrencyType cur : selectedCurrencies) {
		if (currencies != null && !currencies.contains(cur.getUUID()))
			continue;
		if(cur.getCurrencyType() != CurrencyType.Type.CURRENCY)
			continue;
		List<CurrencySnapshot> snapshots = cur.getSnapshots();
		for (CurrencySnapshot snap : snapshots) {
			if (snap.getDateInt() < fromDate || snap.getDateInt()>toDate)
				continue;
			CurrencyRateBean bean= new CurrencyRateBean();
			bean.setSelection(output.getSelection());
			bean.setBean(cur,snap);
			bean.populateData();
			try {
				output.writeRecord(bean);
			}
			catch (RWException e) {
				throw e;
			}
		}
	}
}
}
