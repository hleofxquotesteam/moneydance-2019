package com.moneydance.modules.features.reportwriter.factory;

import java.util.List;
import java.util.SortedMap;

import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.Budget;
import com.infinitekind.moneydance.model.BudgetList;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.BudgetBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.DataParameter;

public class BudgetFactory {

	private DataDataRow dataParams;
	private BudgetList budList;
	private List<Budget> allBudgets;
	private List<String> selectedBudgets=null;
	private SortedMap<String, DataParameter> map;
	private AccountBook book;
	public BudgetFactory(AccountBook book, DataDataRow dataParams,OutputFactory output) throws RWException {
		this.book = book;
		this.dataParams = dataParams;
		map = this.dataParams.getParameters();
		if (map.containsKey(Constants.PARMBUDGET)) 
			selectedBudgets = map.get(Constants.PARMBUDGET).getList();
		if (map.containsKey(Constants.PARMSELBUDGET) && (selectedBudgets == null || selectedBudgets.isEmpty()))
				return;
		budList = this.book.getBudgets();
		if (budList == null)
			return;
		allBudgets = budList.getAllBudgets();
		if (allBudgets == null)
			return;
		for (Budget bud : allBudgets) {
			if (selectedBudgets != null && !selectedBudgets.contains(bud.getUUID()))
				continue;
			BudgetBean bean= new BudgetBean();
			bean.setSelection(output.getSelection());
			bean.setBudget(bud);
			bean.populateData();
			try {
				output.writeRecord(bean);
			}
			catch (RWException e) {
				throw e;
			}
		}
	}
}
