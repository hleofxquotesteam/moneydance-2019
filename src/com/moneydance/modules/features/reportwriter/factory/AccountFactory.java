package com.moneydance.modules.features.reportwriter.factory;

import java.util.List;
import java.util.SortedMap;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AccountUtil;
import com.infinitekind.moneydance.model.AcctFilter;
import com.infinitekind.util.DateUtil;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.AccountBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.DataParameter;

public class AccountFactory implements AcctFilter{

	private DataDataRow dataParams;
	private List<Account> selectedAccts;
	private List<String> accounts=null;
	private SortedMap<String, DataParameter> map;
	private Boolean filterByAcct = false;
	private AccountBook book;
	private int fromDate;
	private int toDate;
	public AccountFactory(AccountBook bookp, DataDataRow dataParamsp,OutputFactory output) throws RWException {
		book = bookp;
		dataParams = dataParamsp;
		map = dataParams.getParameters();
		if (map.containsKey(Constants.PARMFROMDATE))
			fromDate = Integer.valueOf(map.get(Constants.PARMFROMDATE).getValue());
		else 
			fromDate = DateUtil.getStrippedDateInt();
		if (map.containsKey(Constants.PARMTODATE))
			toDate = Integer.valueOf(map.get(Constants.PARMTODATE).getValue());
		else 
			toDate = DateUtil.getStrippedDateInt();
		filterByAcct = map.containsKey(Constants.PARMSELACCT);
		if (map.containsKey(Constants.PARMACCOUNTS)) 
			accounts = map.get(Constants.PARMACCOUNTS).getList();
		selectedAccts = AccountUtil.allMatchesForSearch(book, this);
		for (Account acct : selectedAccts) {
			AccountBean bean= new AccountBean();
			bean.setSelection(output.getSelection());
			bean.setAccount(acct);
			bean.populateData();
			try {
				output.writeRecord(bean);
			}
			catch (RWException e) {
				throw e;
			}
		}
	}
	@Override
	public boolean matches(Account paramAccount) {
		if (paramAccount.getCreationDateInt() > toDate)
			return false;
		if (paramAccount.getAccountIsInactive())
			return false;
		if (!filterByAcct) {
			switch (paramAccount.getAccountType()) {
			case ASSET:
			case BANK:
			case CREDIT_CARD:
			case INVESTMENT:
			case LIABILITY:
			case LOAN:
				return true;
			default:
				return false;
			
			}
		}
		if (accounts != null) {
			if (accounts.contains(paramAccount.getUUID()))
					return true;
			return false;
		}
		switch (paramAccount.getAccountType()) {
		case ASSET:
			if (map.containsKey(Constants.PARMASSET))
				return true;
			return false;
		case BANK:
			if (map.containsKey(Constants.PARMBANK))
				return true;
			return false;
		case CREDIT_CARD:
			if (map.containsKey(Constants.PARMCREDIT))
				return true;
			return false;
		case EXPENSE:
			return false;
		case INCOME:
			return false;
		case INVESTMENT:
			if (map.containsKey(Constants.PARMINVESTMENT))
				return true;
			return false;
		case LIABILITY:
			if (map.containsKey(Constants.PARMLIABILITY))
				return true;
			return false;
		case LOAN:
			if (map.containsKey(Constants.PARMLOAN))
				return true;
			return false;
		case ROOT:
			return false;
		case SECURITY:
			return false;
		default:
			break;		
		}
		return false;
	}
	@Override
	public String format(Account paramAccount) {
		return paramAccount.getUUID();
	}
}
