package com.moneydance.modules.features.reportwriter.factory;

import java.util.List;
import java.util.SortedMap;

import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.CurrencySnapshot;
import com.infinitekind.moneydance.model.CurrencyTable;
import com.infinitekind.moneydance.model.CurrencyType;
import com.infinitekind.util.DateUtil;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.SecurityPriceBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.DataParameter;

public class SecurityPriceFactory {
	private DataDataRow dataParams;
	private CurrencyTable curTable;
	private List<CurrencyType> allSecurities;
	private List<String> selectedSecurities=null;
	private SortedMap<String, DataParameter> map;
	private AccountBook book;
	private int fromDate;
	private int toDate;

public SecurityPriceFactory(AccountBook bookp, DataDataRow dataParamsp,OutputFactory output) throws RWException {
	book = bookp;
	dataParams = dataParamsp;
	map = dataParams.getParameters();
	if (map.containsKey(Constants.PARMSECURITY)) 
		selectedSecurities = map.get(Constants.PARMSECURITY).getList();
	if (map.containsKey(Constants.PARMFROMDATE))
		fromDate = Integer.valueOf(map.get(Constants.PARMFROMDATE).getValue());
	else 
		fromDate = DateUtil.getStrippedDateInt();
	if (map.containsKey(Constants.PARMTODATE))
		toDate = Integer.valueOf(map.get(Constants.PARMTODATE).getValue());
	else 
		toDate = DateUtil.getStrippedDateInt();
	curTable = book.getCurrencies();
	if (curTable == null)
		return;
	allSecurities= curTable.getAllCurrencies();
	if (allSecurities == null)
		return;
	for (CurrencyType cur : allSecurities) {
		if(cur.getCurrencyType() != CurrencyType.Type.SECURITY)
			continue;
		if (selectedSecurities != null && !selectedSecurities.contains(cur.getUUID()))
			continue;
		if (map.containsKey(Constants.PARMSELSECURITY)&& selectedSecurities== null)
			continue;
		List<CurrencySnapshot> snapshots = cur.getSnapshots();
		for (CurrencySnapshot snap : snapshots) {
			if (snap.getDateInt() < fromDate || snap.getDateInt()>toDate)
				continue;
			SecurityPriceBean bean= new SecurityPriceBean();
			bean.setSelection(output.getSelection());
			bean.setBean(cur, snap);
			bean.populateData();
			try {
				output.writeRecord(bean);
			}
			catch (RWException e) {
				throw e;
			}
		}
	}
}
}
