package com.moneydance.modules.features.reportwriter.factory;

import java.util.List;
import java.util.SortedMap;

import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.CurrencyTable;
import com.infinitekind.moneydance.model.CurrencyType;
import com.infinitekind.util.DateUtil;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.SecurityBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.DataParameter;

	public class SecurityFactory {
		private DataDataRow dataParams;
		private CurrencyTable curTable;
		private List<CurrencyType> allSecurities;
		private List<String> selectedSecurities=null;
		private SortedMap<String, DataParameter> map;
		private AccountBook book;
	
	public SecurityFactory(AccountBook bookp, DataDataRow dataParamsp,OutputFactory output) throws RWException {
		book = bookp;
		dataParams = dataParamsp;
		map = dataParams.getParameters();
		if (map.containsKey(Constants.PARMSECURITY)) 
			selectedSecurities = map.get(Constants.PARMSECURITY).getList();
		curTable = book.getCurrencies();
		allSecurities= curTable.getAllCurrencies();
		for (CurrencyType cur : allSecurities) {
			if(cur.getCurrencyType() != CurrencyType.Type.SECURITY)
				continue;
			if (selectedSecurities != null && !selectedSecurities.contains(cur.getUUID()))
				continue;
			if (map.containsKey(Constants.PARMSELSECURITY)&& selectedSecurities== null)
				continue;
			SecurityBean bean= new SecurityBean();
			bean.setSelection(output.getSelection());
			bean.setSecurity(cur);
			bean.populateData();
			try {
				output.writeRecord(bean);
			}
			catch (RWException e) {
				throw e;
			}
		}
	}
}
