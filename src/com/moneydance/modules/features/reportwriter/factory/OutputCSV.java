package com.moneydance.modules.features.reportwriter.factory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import com.moneydance.modules.features.reportwriter.Parameters;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.DataBean;
import com.moneydance.modules.features.reportwriter.view.ReportDataRow;

public class OutputCSV extends OutputFactory {
	private File outputfile;
	private PrintWriter writer;
	private String dataDirectory;
	private String delimiter;
	public OutputCSV(ReportDataRow report, Parameters params) throws RWException {
		super(report, params);
	}
	@Override
	public boolean chooseFile(String fileName) throws RWException {
		File directory;
		File [] files=null;
		boolean exists=false;
		this.fileName = fileName;
		dataDirectory = params.getDataDirectory();
		directory = new File(dataDirectory);
		if (directory != null)
			files = directory.listFiles();
		for (File temp : files) {
			String [] parts = temp.getName().split("[.]");
			if (parts[0].equalsIgnoreCase(fileName) && parts[parts.length-1].equalsIgnoreCase("csv")) {
				exists = true;
				if (report.getOverWrite())
					temp.delete();
			}
		}
		if (!report.getOverWrite() && exists)
				throw new RWException ("csv files already exists");
		delimiter = report.getDelimiter();
		if (delimiter.equals("Tab"))
			delimiter = "\t";
		return true;
	}

	@Override
	public void createRecord(DataBean bean) throws RWException {
		try {
			outputfile = new File(dataDirectory+"/"+fileName+"."+bean.getShortTableName()+".csv");
			writer = new PrintWriter(outputfile);
		}
		catch (IOException e) {
			throw new RWException("IO Error on "+dataDirectory+"/"+fileName+".acct.csv");
		}
		String[] fields = bean.createHeaderArray();
		String line="";
		for (int ii=0;ii<fields.length;ii++) {
			fields[ii] = escape(fields[ii]);
			line += fields[ii];
			if (ii<fields.length-1)
				line+=delimiter;
		}
		writer.println(line);

	}
	@Override
	public void writeRecord (DataBean bean) throws RWException{
		String[] fields = bean.createStringArray();
		String line="";
		for (int ii=0;ii<fields.length;ii++) {
			fields[ii] = escape(fields[ii]);
			line += fields[ii];
			if (ii<fields.length-1)
				line+=delimiter;
		}
		writer.println(line);
	}
	@Override
	public void closeRecordFile() throws RWException{
		if (writer != null) {
			writer.close();
		}
		writer = null;
	}
	  private String escape(String str) {
		  if (str == null)
			  return null;
		  if(str.contains(delimiter))
		  		return str.replace(delimiter," ");
	      return str;
	  }


}
