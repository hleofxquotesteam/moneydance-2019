package com.moneydance.modules.features.reportwriter.factory;

import java.util.List;
import java.util.SortedMap;

import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.Reminder;
import com.infinitekind.moneydance.model.ReminderSet;
import com.infinitekind.util.DateUtil;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.ReminderBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.DataParameter;

public class ReminderFactory {
	private DataDataRow dataParams;
	private ReminderSet reminders;
	private List<Reminder> selectedReminders;
	private SortedMap<String, DataParameter> map;
	private AccountBook book;
	private int fromDate;
	private int toDate;
	public ReminderFactory(AccountBook book, DataDataRow dataParams,OutputFactory output) throws RWException {
		this.book = book;
		this.dataParams = dataParams;
		map = this.dataParams.getParameters();
		reminders = this.book.getReminders();
		if (reminders == null)
			return;
		selectedReminders = reminders.getAllReminders();
		if (selectedReminders == null)
			return;
		if (map.containsKey(Constants.PARMFROMDATE))
			fromDate = Integer.valueOf(map.get(Constants.PARMFROMDATE).getValue());
		else 
			fromDate = DateUtil.getStrippedDateInt();
		if (map.containsKey(Constants.PARMTODATE))
			toDate = Integer.valueOf(map.get(Constants.PARMTODATE).getValue());
		else 
			toDate = DateUtil.getStrippedDateInt();	
		for (Reminder rem : selectedReminders) {
			if (rem.getInitialDateInt()> toDate)
				continue;
			if (rem.getLastDateInt() > 0 && rem.getLastDateInt()<fromDate)
				continue;
			ReminderBean bean= new ReminderBean();
			bean.setSelection(output.getSelection());
			bean.setReminder(rem);
			bean.populateData();
			try {
				output.writeRecord(bean);
			}
			catch (RWException e) {
				throw e;
			}
		}
	}
}
