package com.moneydance.modules.features.reportwriter.factory;
import java.util.List;
import java.util.SortedMap;

import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.CurrencyTable;
import com.infinitekind.moneydance.model.CurrencyType;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.CurrencyBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.DataParameter;

	public class CurrencyFactory {
		private DataDataRow dataParams;
		private CurrencyTable curTable;
		private List<CurrencyType> allCurrencies;
		private List<String> selectedCurrencies=null;
		private SortedMap<String, DataParameter> map;
		private AccountBook book;
	
	public CurrencyFactory(AccountBook bookp, DataDataRow dataParamsp,OutputFactory output) throws RWException {
		book = bookp;
		dataParams = dataParamsp;
		map = dataParams.getParameters();
		if (map.containsKey(Constants.PARMCURRENCY)) 
			selectedCurrencies = map.get(Constants.PARMCURRENCY).getList();
		curTable = book.getCurrencies();
		allCurrencies= curTable.getAllCurrencies();
		for (CurrencyType cur : allCurrencies) {
			if(cur.getCurrencyType() != CurrencyType.Type.CURRENCY)
				continue;
			if (selectedCurrencies != null && !selectedCurrencies.contains(cur.getUUID()))
				continue;
			if (map.containsKey(Constants.PARMSELCURRENCY) && selectedCurrencies == null)
				continue;
			CurrencyBean bean= new CurrencyBean();
			bean.setSelection(output.getSelection());
			bean.setCurrency(cur);
			bean.populateData();
			try {
				output.writeRecord(bean);
			}
			catch (RWException e) {
				throw e;
			}
		}
	}
}
