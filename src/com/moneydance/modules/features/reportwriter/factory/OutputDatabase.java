package com.moneydance.modules.features.reportwriter.factory;

import com.moneydance.modules.features.reportwriter.Database;
import com.moneydance.modules.features.reportwriter.Parameters;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.DataBean;
import com.moneydance.modules.features.reportwriter.view.ReportDataRow;

public class OutputDatabase extends OutputFactory {
	private Database database;
	private String filename;
	private boolean databaseClosed = true;
	public OutputDatabase(ReportDataRow reportp, Parameters paramsp) throws RWException {
		super(reportp, paramsp);
	}
	@Override
	public boolean chooseFile(String filename) throws RWException {
		try {
			this.filename = filename;
			database = new Database(params,filename,report.getOverWrite());
			databaseClosed = false;
		}
		catch (RWException e) {
			throw new RWException(e.getLocalizedMessage());
		}
		return true;
	}

	@Override
	public void closeOutputFile() throws RWException{
		if (!databaseClosed) {
			database.commit();
		}
		databaseClosed = true;
	}
	@Override
	public void createRecord(DataBean bean) throws RWException {
		database.createTable(bean);
	}
	@Override
	public void closeRecordFile() throws RWException {
		database.commit();		
	}
	@Override
	public void writeRecord (DataBean bean) throws RWException{
		String sql = bean.createSQL();
		database.executeUpdate(sql);
	}
	public Database getDatabase() throws RWException {
		try {
			if (databaseClosed && database == null) {
				database = new Database(params,filename,report.getOverWrite());
				databaseClosed = false;
			}
			return database;
		}
		catch (RWException e) {
			throw new RWException(e.getLocalizedMessage());
		}
	}

}
