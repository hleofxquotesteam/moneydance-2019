package com.moneydance.modules.features.reportwriter.factory;

import java.util.List;
import java.util.SortedMap;

import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AccountUtil;
import com.infinitekind.moneydance.model.AcctFilter;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.CategoryBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.DataParameter;

public class CategoryFactory implements AcctFilter {
	private DataDataRow dataParams;
	private List<Account> allCats;
	private List<String> selectedCats=null;
	private SortedMap<String, DataParameter> map;
	private AccountBook book;
	private Boolean allExpense = false;
	private Boolean allIncome = false;
	private Boolean specified = false;
	public CategoryFactory(AccountBook bookp, DataDataRow dataParamsp,OutputFactory output) throws RWException {
		book = bookp;
		dataParams = dataParamsp;
		map = dataParams.getParameters();
		if (map.containsKey(Constants.PARMCATEGORIES)) 
			selectedCats = map.get(Constants.PARMCATEGORIES).getList();
		if (selectedCats != null)
			specified = true;
		else {
			if (map.containsKey(Constants.PARMINCOME))
				allIncome = true;
			if (map.containsKey(Constants.PARMEXPENSE))
				allExpense = true;
		}
		allCats = AccountUtil.allMatchesForSearch(book, this);
		for (Account acct : allCats) {
			CategoryBean bean= new CategoryBean();
			bean.setSelection(output.getSelection());
			bean.setCategory(acct);
			bean.populateData();
			try {
				output.writeRecord(bean);
			}
			catch (RWException e) {
				throw e;
			}
		}
	}
	@Override
	public boolean matches(Account paramAccount) {
		if (!(paramAccount.getAccountType() == AccountType.INCOME || paramAccount.getAccountType() == AccountType.EXPENSE))
			return false;
		if (specified) {
			if (selectedCats.contains(paramAccount.getUUID()))
				return true;
			return false;
		}
		if (map.containsKey(Constants.PARMSELCAT)&&!allIncome&&!allExpense)
			return false;
		if (allIncome && paramAccount.getAccountType() == AccountType.INCOME)
			return true;
		if (allExpense && paramAccount.getAccountType() == AccountType.EXPENSE)
			return true;
		if (!specified && !allIncome && !allExpense)
			return true;
		return false;
	}
	@Override
	public String format(Account paramAccount) {
		return paramAccount.getUUID();
	}
}
