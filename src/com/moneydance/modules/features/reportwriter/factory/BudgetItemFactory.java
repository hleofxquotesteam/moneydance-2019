package com.moneydance.modules.features.reportwriter.factory;

import java.util.List;
import java.util.SortedMap;

import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.Budget;
import com.infinitekind.moneydance.model.BudgetItem;
import com.infinitekind.moneydance.model.BudgetItemList;
import com.infinitekind.moneydance.model.BudgetList;
import com.infinitekind.moneydance.model.DateRange;
import com.infinitekind.util.DateUtil;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.BudgetBean;
import com.moneydance.modules.features.reportwriter.databeans.BudgetItemBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.DataParameter;

public class BudgetItemFactory {
	private DataDataRow dataParams;
	private BudgetList budList;
	private List<Budget> allBudgets;
	private List<String> selectedBudgets=null;
	private SortedMap<String, DataParameter> map;
	private AccountBook book;
	private int fromDate;
	private int toDate;
	public BudgetItemFactory(AccountBook bookp, DataDataRow dataParamsp,OutputFactory output) throws RWException {
		book = bookp;
		dataParams = dataParamsp;
		map = dataParams.getParameters();
		if (map.containsKey(Constants.PARMBUDGET)) 
			selectedBudgets = map.get(Constants.PARMBUDGET).getList();
		if (map.containsKey(Constants.PARMFROMDATE))
			fromDate = Integer.valueOf(map.get(Constants.PARMFROMDATE).getValue());
		else 
			fromDate = DateUtil.getStrippedDateInt();
		if (map.containsKey(Constants.PARMTODATE))
			toDate = Integer.valueOf(map.get(Constants.PARMTODATE).getValue());
		else 
			toDate = DateUtil.getStrippedDateInt();	
		if (map.containsKey(Constants.PARMSELBUDGET) && (selectedBudgets == null || selectedBudgets.isEmpty()))
			return;
		budList = book.getBudgets();
		allBudgets = budList.getAllBudgets();
		for (Budget bud : allBudgets) {
			if (selectedBudgets != null && !selectedBudgets.contains(bud.getUUID()))
				continue;
			BudgetItemList items = bud.getItemList();
			for (int i=0;i<items.getItemCount();i++) {
				BudgetItem item = items.getItem(i);
				DateRange range = item.getDateRange();
				if (range.getStartDateInt() > toDate  || range.getEndDateInt() < fromDate)
					continue;
				BudgetItemBean bean= new BudgetItemBean();
				bean.setSelection(output.getSelection());
				bean.setBean(bud,item);
				bean.populateData();
				try {
					output.writeRecord(bean);
				}
				catch (RWException e) {
					throw e;
				}

			}
		}
	}
}
