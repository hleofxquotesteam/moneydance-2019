package com.moneydance.modules.features.reportwriter.factory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.view.ReportDataRow;
import com.moneydance.modules.features.reportwriter.Parameters;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.DataBean;

public class OutputSpreadsheet extends OutputFactory {
	private String dataDirectory;
	private File workBookFile;
	private OutputStream fileOut;
	private Workbook workBook;
	private Sheet workSheet;
	private int rowIndex=2;
	private BEANFIELDTYPE [] columnTypes;
	private CellStyle integerStyle;
	private CellStyle numberStyle;
	public OutputSpreadsheet(ReportDataRow reportp, Parameters paramsp) throws RWException {
		super(reportp, paramsp);
	}
	@Override
	public boolean chooseFile(String fileNamep) throws RWException {
		fileName = fileNamep;
		dataDirectory = params.getDataDirectory();
		workBookFile = new File(dataDirectory+"/"+fileName+".xlsx");
		
		if(workBookFile.exists()) {
			if (report.getOverWrite())
				workBookFile.delete();
			else
				throw new RWException (" csv files already exists");				
		}
		workBook = new XSSFWorkbook();
		try {
			fileOut = new FileOutputStream(dataDirectory+"/"+fileName+".xlsx");
		}
		catch (IOException e) {
			throw new RWException("Error creating workbook "+fileName+".xlsx");
		}
		integerStyle = workBook.createCellStyle();
		integerStyle.setDataFormat((short)1);
		numberStyle = workBook.createCellStyle();
		numberStyle.setDataFormat((short)2);
		return true;
	}

	@Override
	public void closeOutputFile() throws RWException {
		if (workBook != null ) {
			try {
				try {
					workBook.write(fileOut);
				}
				catch (IOException e) {
					throw new RWException("Error writing workbook "+fileName+".xlsx");
				}
				fileOut.flush();
				fileOut.close();
				workBook.close();
			}
			catch (IOException e) {
				throw new RWException("Error closing workbook "+fileName+".xlsx");
			}
		}
		workBook = null;
	}
	@Override
	public void createRecord(DataBean bean) throws RWException {
		workSheet= workBook.createSheet(bean.getTableName());
		Row headerRow = workSheet.createRow(1);
		rowIndex=2;
		String [] headFields = bean.createHeaderArray();
		columnTypes = bean.getColumnTypes();
		int index =0;
		for (String field : headFields) {
			if (field.isEmpty())
				index++;
			else {
				Cell cell = headerRow.createCell(index++);
				cell.setCellValue(field);
			}
		}
	}
	@Override
	public void writeRecord (DataBean bean) throws RWException{
		String[] fields = bean.createStringArray();
		Row dataRow = workSheet.createRow(rowIndex++);
		int index =0;
		for (int i=0;i<fields.length;i++) {
			String field = fields[i];
			if (field.isEmpty())
				index++;
			else {
				Cell cell = dataRow.createCell(index++);
				setField(cell, dataRow, field, columnTypes[i]);
			}
		}
	}
	@Override
	public void closeRecordFile() throws RWException{

	}
	private void setField(Cell cell, Row dataRow, String field, BEANFIELDTYPE type) {
		switch (type) {
		case BOOLEAN:
			boolean tempBool =Boolean.valueOf(field); 
			cell.setCellValue(tempBool);
			break;
		case DATEINT:
			Date tempDate = Date.valueOf(field);
			cell.setCellValue(tempDate);
			break;
		case INTEGER:
			int tempInt = Integer.valueOf(field);
			cell.setCellValue(tempInt);
			cell.setCellStyle(integerStyle);
			break;
		case MONEY:
		case DOUBLE:
		case PERCENT:
			double tempDouble =Double.valueOf(field);
			cell.setCellValue(tempDouble);
			cell.setCellStyle(numberStyle);
			break;
		default:
			cell.setCellValue(field);
			break;
		}

	}
}
