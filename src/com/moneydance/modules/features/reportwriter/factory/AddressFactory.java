package com.moneydance.modules.features.reportwriter.factory;

import java.util.List;

import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AddressBook;
import com.infinitekind.moneydance.model.AddressBookEntry;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.AddressBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;

public class AddressFactory {
	private AddressBook addrTable;
	private List<AddressBookEntry> selectedAddresses;
	private AccountBook book;

public AddressFactory(AccountBook bookp, DataDataRow dataParamsp,OutputFactory output) throws RWException {
	book = bookp;
	addrTable = book.getAddresses();
	selectedAddresses= addrTable.getAllEntries();
	for (AddressBookEntry addr : selectedAddresses) {
		AddressBean bean= new AddressBean();
		bean.setSelection(output.getSelection());
		bean.setAddress(addr);
		bean.populateData();
		try {
			output.writeRecord(bean);
		}
		catch (RWException e) {
			throw e;
		}
	}
}
}
