package com.moneydance.modules.features.reportwriter.factory;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import com.moneydance.modules.features.reportwriter.Main;
import com.moneydance.modules.features.reportwriter.Parameters;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.Utilities;
import com.moneydance.modules.features.reportwriter.databeans.AccountBean;
import com.moneydance.modules.features.reportwriter.databeans.AddressBean;
import com.moneydance.modules.features.reportwriter.databeans.BudgetBean;
import com.moneydance.modules.features.reportwriter.databeans.BudgetItemBean;
import com.moneydance.modules.features.reportwriter.databeans.CategoryBean;
import com.moneydance.modules.features.reportwriter.databeans.CurrencyBean;
import com.moneydance.modules.features.reportwriter.databeans.CurrencyRateBean;
import com.moneydance.modules.features.reportwriter.databeans.DataBean;
import com.moneydance.modules.features.reportwriter.databeans.InvTranBean;
import com.moneydance.modules.features.reportwriter.databeans.ReminderBean;
import com.moneydance.modules.features.reportwriter.databeans.SecurityBean;
import com.moneydance.modules.features.reportwriter.databeans.SecurityPriceBean;
import com.moneydance.modules.features.reportwriter.databeans.TransactionBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.ReportDataRow;
import com.moneydance.modules.features.reportwriter.view.SelectionDataRow;

public abstract class OutputFactory {
	protected String fileName;
	protected String name;
	protected Parameters params;
	protected DataDataRow data;
	protected SelectionDataRow selection;
	protected ReportDataRow report;
	public OutputFactory(ReportDataRow reportp, Parameters paramsp) throws RWException {
		report = reportp;
		name = report.getName();
		params = paramsp;

		selection = new SelectionDataRow();
		if (!selection.loadRow(report.getSelection(), params)){
			JOptionPane.showMessageDialog(null,"Selection Group file "+report.getSelection()+" not found");
			return;
		}
		data = new DataDataRow();
		if(!data.loadRow(report.getDataParms(), params)){
			JOptionPane.showMessageDialog(null,"Data Parameters file "+report.getDataParms()+" not found");
			return;				
		}
			String filename;
		String now = new SimpleDateFormat("yyyy-MM-ddHHmmss").format(new Date());
		if (report.getGenerate()) 
			filename = "ReportWriter"+ now;
		else
			filename=report.getOutputFileName();
		if (report.getAddDate())
			filename += now;
		try {
				if (chooseFile(filename)) {
					createOutput();			
					selection.touchFile();
					data.touchFile();
				}
		}
		catch (RWException e) {
			JOptionPane.showMessageDialog(null,"Error creating "+report.getName()+" "+e.getLocalizedMessage());
			throw e;
		}
	}
	public SelectionDataRow getSelection() {
		return selection;
	}
	public boolean chooseFile(String filename) throws RWException {
		return false;
	}
	public void createOutput()  throws RWException{
		try {
			if (selection.getAccounts()) {
				AccountBean acct = new AccountBean();
				acct.setSelection(selection);
				createRecord(acct);
				Utilities.notifyUser("Account Record Created");
				AccountFactory acctFact = new AccountFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Account Records Output");
				closeRecordFile();
			}
			if (selection.getAddress()) {
				AddressBean addr = new AddressBean();
				addr.setSelection(selection);
				createRecord(addr);
				Utilities.notifyUser("Address Record Created");
				AddressFactory addrFact = new AddressFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Address Records Output");
				closeRecordFile();
			}
			if (selection.getBudgets()) {
				BudgetBean bud = new BudgetBean();
				bud.setSelection(selection);
				createRecord(bud);
				Utilities.notifyUser("Budget Record Created");
				BudgetFactory budFact = new BudgetFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Budget Records Output");
				closeRecordFile();
			}
			if (selection.getTransactions()) {
				TransactionBean tran = new TransactionBean();
				tran.setSelection(selection);
				createRecord(tran);
				Utilities.notifyUser("Transaction Record Created");
				TransactionFactory tranFact = new TransactionFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Transaction Records Output");
				closeRecordFile();
			}
			if (selection.getInvTrans()) {
				InvTranBean tran = new InvTranBean();
				tran.setSelection(selection);;
				createRecord(tran);
				Utilities.notifyUser("Investment Transaction Record Created");
				InvestTranFactory tranFact = new InvestTranFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Investment Transaction Records Output");
				closeRecordFile();
			}
			if (selection.getCurrencies()) {
				CurrencyBean cur = new CurrencyBean();
				cur.setSelection(selection);
				createRecord(cur);
				Utilities.notifyUser("Currency Record Created");
				CurrencyFactory curFact = new CurrencyFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Currency Records Output");
				closeRecordFile();
			}
			if (selection.getSecurities()) {
				SecurityBean sec = new SecurityBean();
				sec.setSelection(selection);
				createRecord(sec);
				Utilities.notifyUser("Security Record Created");
				SecurityFactory secFact = new SecurityFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Security Records Output");
				closeRecordFile();
			}
			if (selection.getCategories()) {
				CategoryBean cat = new CategoryBean();
				cat.setSelection(selection);
				createRecord(cat);
				Utilities.notifyUser("Category Record Created");
				CategoryFactory catFact = new CategoryFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Category Records Output");
				closeRecordFile();
			}
			if (selection.getSecurityPrices()) {
				SecurityPriceBean secp = new SecurityPriceBean();
				secp.setSelection(selection);
				createRecord(secp);
				Utilities.notifyUser("Security Price Record Created");
				SecurityPriceFactory secFact = new SecurityPriceFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Security Price Records Output");
				closeRecordFile();
			}
			if (selection.getCurrencyRates()) {
				CurrencyRateBean curr = new CurrencyRateBean();
				curr.setSelection(selection);
				createRecord(curr);
				Utilities.notifyUser("Currency Rate Record Created");
				CurrencyRateFactory secFact = new CurrencyRateFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Currency Rate Records Output");
				closeRecordFile();
			}
			if (selection.getBudgetItems()) {
				BudgetItemBean budi = new BudgetItemBean();
				budi.setSelection(selection);
				createRecord(budi);
				Utilities.notifyUser("Budget Item Record Created");
				BudgetItemFactory budiFact = new BudgetItemFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Budget Item Records Output");
				closeRecordFile();
			}
			if (selection.getReminders()) {
				ReminderBean rem = new ReminderBean();
				rem.setSelection(selection);
				createRecord(rem);
				Utilities.notifyUser("Reminder Record Created");
				ReminderFactory budiFact = new ReminderFactory(Main.context.getCurrentAccountBook(),data,this);
				Utilities.notifyUser("Reminder Records Output");
				closeRecordFile();
			}
			closeOutputFile();
			JOptionPane.showMessageDialog(null,"Report/File "+name+" created");	
		}
		catch (RWException e) {
			throw e;
		}
	}
	public void createRecord(DataBean bean) throws RWException{
		
	}
	public void writeRecord(DataBean bean) throws RWException {
	}
	public void closeOutputFile() throws RWException {
		
	}
	public void closeRecordFile() throws RWException {
		
	}

}
