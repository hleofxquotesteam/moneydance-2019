package com.moneydance.modules.features.reportwriter.factory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.InvestFields;
import com.infinitekind.moneydance.model.SplitTxn;
import com.infinitekind.moneydance.model.TransactionSet;
import com.infinitekind.moneydance.model.Txn;
import com.infinitekind.moneydance.model.TxnSearch;
import com.infinitekind.moneydance.model.TxnSet;
import com.infinitekind.moneydance.model.AbstractTxn.ClearedStatus;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.tiksync.SyncRecord;
import com.infinitekind.util.DateUtil;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.RWException;
import com.moneydance.modules.features.reportwriter.databeans.TransactionBean;
import com.moneydance.modules.features.reportwriter.view.DataDataRow;
import com.moneydance.modules.features.reportwriter.view.DataParameter;

public class InvestTranFactory implements TxnSearch {
	private DataDataRow dataParams;
	private List<String> accounts=null;
	private List<String> invAccounts=null;
	private List<String> securities=null;
	private List<String> categories=null;
	private List<String> tags=null;
	private List<String> transfers=null;
	private SortedMap<String, DataParameter> map;
	private TransactionSet txns;
	private TxnSet selected;
	private Iterator<AbstractTxn>iter;
	private AccountBook book;
	private int fromDate;
	private int toDate;
	public InvestTranFactory(AccountBook bookp, DataDataRow dataParamsp,OutputFactory output) throws RWException{
		book = bookp;
		dataParams = dataParamsp;
		map = dataParams.getParameters();
		if (map.containsKey(Constants.PARMFROMDATE))
			fromDate = Integer.valueOf(map.get(Constants.PARMFROMDATE).getValue());
		else 
			fromDate = DateUtil.getStrippedDateInt();
		if (map.containsKey(Constants.PARMTODATE))
			toDate = Integer.valueOf(map.get(Constants.PARMTODATE).getValue());
		else 
			toDate = DateUtil.getStrippedDateInt();
		if (map.containsKey(Constants.PARMACCOUNTS)) 
			accounts = map.get(Constants.PARMACCOUNTS).getList();
		if (map.containsKey(Constants.PARMINVACCTS)) {
			invAccounts = map.get(Constants.PARMINVACCTS).getList();
			for (String acct : invAccounts) {
				Account tempAcct = book.getAccountByUUID(acct);
				if (tempAcct != null && tempAcct.getAccountType() == AccountType.SECURITY) {
					if (securities == null)
						securities = new ArrayList<String>();
					securities.add(acct);
				}
			}
			if (securities != null) {
				for (String sec:securities)
					invAccounts.remove(sec);
			}
		}
		if (map.containsKey(Constants.PARMCATEGORIES))
			categories = map.get(Constants.PARMCATEGORIES).getList();
		if (map.containsKey(Constants.PARMTAGS))
			tags = map.get(Constants.PARMTAGS).getList();
		if (map.containsKey(Constants.PARMTRANSFER))
			transfers = map.get(Constants.PARMTRANSFER).getList();
		txns = book.getTransactionSet();
		selected = txns.getTransactions(this);
		iter = selected.iterator();
		while(iter.hasNext()) {
			TransactionBean bean = new TransactionBean();
			bean.setSelection(output.getSelection());
			bean.setTrans(iter.next());
			bean.populateData();
			try {
				output.writeRecord(bean);
			}
			catch (RWException e) {
				throw e;
			}
		}
	}
	@Override
	public boolean matches(Txn txn) {
		if (txn.getDateInt() < fromDate)
			return false;
		if (txn.getDateInt() > toDate)
			return false;
		if (txn instanceof SplitTxn) {
			if (txn.getAccount().getAccountType()!=AccountType.INCOME &&
					txn.getAccount().getAccountType()!=AccountType.EXPENSE) {
				if (!testAccount(txn))
					return false;
			}
			if (map.containsKey(Constants.PARMSELCAT) ) {
				if (categories == null) {
					if (txn.getAccount().getAccountType()==AccountType.INCOME && !map.containsKey(Constants.PARMINCOME))
						return false;
					if (txn.getAccount().getAccountType()==AccountType.EXPENSE && !map.containsKey(Constants.PARMEXPENSE))
						return false;
				}
				else {
					if (!categories.contains(txn.getAccount().getUUID()))
						return false;
				}
			}
			/*
			 * Split txn not disqualified by category, test parent to see if it selected
			 */
			if (testParent(txn.getParentTxn()))
				return true;
			return false;
		}
		/*
		 * Parent TXN test filters
		 */
		return testParent(txn);
	}
	private boolean testParent(Txn txn) {
		if (!testAccount(txn))
			return false;
		if (transfers != null)
			if(!transfers.contains(txn.getAccount().getAccountType().name()))
				return false;
		if (map.containsKey(Constants.PARMSELTRANS)) {
			// test 6/7
			if (map.containsKey(Constants.PARMCLEARED)||map.containsKey(Constants.PARMRECON) || map.containsKey(Constants.PARMUNRECON)) {
				if (txn.getClearedStatus() == ClearedStatus.CLEARED && !map.containsKey(Constants.PARMCLEARED))
					return false;
				if (txn.getClearedStatus() == ClearedStatus.RECONCILING && !map.containsKey(Constants.PARMRECON))
					return false;
				if (txn.getClearedStatus() == ClearedStatus.UNRECONCILED && !map.containsKey(Constants.PARMUNRECON))
					return false;
			}
			// test 16
			if (tags != null) {
				SyncRecord sync = ((AbstractTxn)txn).getTags();
				Set<String> setKeys = sync.keySet();
				String[] keys = setKeys.toArray(new String[setKeys.size()]);
				boolean found = false;
				for (String keyitem : keys) {
					if (tags.contains(keyitem))
						found = true;
				}
				if (!found)
					return false;
			}
			InvestFields invest = new InvestFields();
			invest.setFieldStatus(txn);
			if (securities != null && invest.hasSecurity)
				if (!securities.contains(invest.security.getUUID()))
					return false;
			if (map.containsKey(Constants.PARMSELCAT) && invest.hasCategory) {
				Boolean catFound = false;
				if (categories == null) {
					if (invest.category.getAccountType()==AccountType.INCOME && map.containsKey(Constants.PARMINCOME)) 
							catFound = true;
						if (invest.category.getAccountType()==AccountType.EXPENSE && map.containsKey(Constants.PARMEXPENSE)) 
							catFound = true;
				}
				else {
					if (categories.contains(invest.category.getUUID())) 
						catFound = true;
				}					
				if (!catFound)
					return false;
				
			}
		}
		return true;
	}
	private boolean testAccount(Txn txn) {
		// test 3
		if (map.containsKey(Constants.PARMSELACCT)){
			switch (txn.getAccount().getAccountType()) {
			case BANK :
				if (!map.containsKey(Constants.PARMBANK))
					return false;
				break;
			case ASSET:
				if (!map.containsKey(Constants.PARMASSET))
					return false;
				break;
			case CREDIT_CARD:
				if (!map.containsKey(Constants.PARMCREDIT))
					return false;
				break;
			case INVESTMENT:
				if (!map.containsKey(Constants.PARMINVESTMENT))
					return false;
				break;
			case LIABILITY:
				if (!map.containsKey(Constants.PARMLIABILITY))
					return false;
				break;
			case LOAN:
				if (!map.containsKey(Constants.PARMLOAN))
					return false;
				break;
			default:
				break;
			}
			if(accounts != null) {
				if(!accounts.contains(txn.getAccount().getUUID()))
					return false;
			}
		}
		return true;
	}
	@Override
	public boolean matchesAll() {
		// TODO Auto-generated method stub
		return false;
	}
}
