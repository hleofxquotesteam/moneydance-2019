package com.moneydance.modules.features.reportwriter.databeans;

import com.infinitekind.moneydance.model.CurrencySnapshot;
import com.infinitekind.moneydance.model.CurrencyType;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.Utilities;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class SecurityPriceBean extends DataBean {
		@ColumnName("SecurityId")
		@ColumnTitle("Security ID")
		@FieldType(BEANFIELDTYPE.STRING)
		public String currencyId;
		@ColumnName("Ticker")
		@ColumnTitle("Ticker")
		@FieldType(BEANFIELDTYPE.STRING)
		public String ticker;
		@ColumnName("Date")
		@ColumnTitle("Date")
		@FieldType(BEANFIELDTYPE.DATEINT)
		public java.sql.Date dateInt;	
		@ColumnName("DailyHigh")
		@ColumnTitle("Daily High")
		@FieldType(BEANFIELDTYPE.DOUBLE)
		public double dailyHigh;
		@ColumnName("DailyLow")
		@ColumnTitle("Daily Low")
		@FieldType(BEANFIELDTYPE.DOUBLE)
		public double dailyLow;
		@ColumnName("Rate")
		@ColumnTitle("Rate")
		@FieldType(BEANFIELDTYPE.DOUBLE)
		public double rate;
		@ColumnName("DailyVolume")
		@ColumnTitle("Daily Volume")
		@FieldType(BEANFIELDTYPE.LONG)
		public long dailyVolume;
		private CurrencyType security;
		private CurrencySnapshot snapshot;
		public SecurityPriceBean() {
			super();
			tableName = "SecurityPrice";
			shortName = "secp";
			parmsName = Constants.PARMFLDSECP;
		}
		public void setBean(CurrencyType security, CurrencySnapshot snapshot) {
			this.security = security;
			this.snapshot = snapshot;
		}
		@Override
		public void populateData() {
			currencyId = setString(security.getIDString());
			ticker = setString(security.getTickerSymbol());
			dateInt = setDate(Utilities.getSQLDate(snapshot.getDateInt()));
			dailyHigh = setDouble(snapshot.getDailyHigh());
			dailyLow = setDouble(snapshot.getDailyLow());
			rate = setDouble(snapshot.getRate());
			dailyVolume = setLong(snapshot.getDailyVolume());

		}

}
