package com.moneydance.modules.features.reportwriter.databeans;

import java.util.List;

import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.ParentTxn;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.InvestFields;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.Utilities;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class InvTranBean extends DataBean {
	@ColumnName("TxnType")
	@ColumnTitle("Transaction Type")
	@FieldType(BEANFIELDTYPE.STRING)
	public String txnType;
	@ColumnName("ParentTxnID")
	@ColumnTitle("Parent Txn ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String parTxnId;
	@ColumnName("TxnID")
	@ColumnTitle("Txn ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String txnId;
	@ColumnName("AccountName")
	@ColumnTitle("Account Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String account;
	@ColumnName("CheckNum")
	@ColumnTitle("Check Number")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String check;
	@ColumnName("DateEntered")
	@ColumnTitle("Date Entered")
	@FieldType(BEANFIELDTYPE.DATEINT)
	 public java.sql.Date entered;
	@ColumnName("DatePosted")
	@ColumnTitle("Date Posted Online")
	@FieldType(BEANFIELDTYPE.DATEINT)
	 public java.sql.Date datePosted;
	@ColumnName("Description")
	@ColumnTitle("Description")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String description;
	@ColumnName("Status")
	@ColumnTitle("Status")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String status;
	@ColumnName("TaxDate")
	@ColumnTitle("Tax Date")
	@FieldType(BEANFIELDTYPE.DATEINT)
	 public java.sql.Date tax;
	@ColumnTitle("Parent Value")
	@ColumnName("PrntValue")
	@FieldType(BEANFIELDTYPE.MONEY)
	 public long parentValue;
	@ColumnTitle("Split Value")
	@ColumnName("SpltValue")
	@FieldType(BEANFIELDTYPE.MONEY)
	public long splitValue;
	@ColumnTitle("Fee")
	@ColumnName("Fee")
	@FieldType(BEANFIELDTYPE.MONEY)
	 public long fee;
	@ColumnTitle("Price")
	@ColumnName("Price")
	@FieldType(BEANFIELDTYPE.DOUBLE)
	 public double price;
	@ColumnName("TransferType")
	@ColumnTitle("Transfer Type")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String transfer;
	@ColumnName("Tags")
	@ColumnTitle("Tags")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String tags;
	@ColumnName("Memo")
	@ColumnTitle("Medmo")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String memo;
	@ColumnName("Category")
	@ColumnTitle("Category")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String category;
	@ColumnName("FeeAcct")
	@ColumnTitle("Fee Account")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String feeAcct;
	@ColumnName("SplitType")
	@ColumnTitle("Split Type")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String splitType;
	@ColumnTitle("Transfer Account")
	@ColumnName("TransAcct")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String transAcct;
	@ColumnName("Security")
	@ColumnTitle("Security")
	@FieldType(BEANFIELDTYPE.STRING)
	public String security;
	@ColumnName("NumShares")
	@ColumnTitle("Number Shares")
	@FieldType(BEANFIELDTYPE.LONG)
	public long numShares;


	
	AbstractTxn trans;
	
	public InvTranBean () {
		super();
		tableName = "InvTrans";
		shortName = "invtrans";
		parmsName = Constants.PARMFLDINVTRAN;
		
	}
	public AbstractTxn getTrans() {
		return trans;
	}
	public void setTrans(AbstractTxn trans) {
		this.trans = trans;
	}

	@Override
	public void populateData() {
		ParentTxn pTxn=trans.getParentTxn();
		InvestFields invest = new InvestFields();
		invest.setFieldStatus(pTxn);
		if (trans instanceof ParentTxn) {
			txnType = "P";
			account = setString(trans.getAccount()==null?"":trans.getAccount().getAccountName());
			category = "";
			parentValue = setMoney(trans.getValue());
			splitValue = 0L;
			memo = ((ParentTxn)trans).getMemo();
			splitType = "";
		}
		else {
			txnType = "S";
			account = "";
			if (trans.getAccount().getAccountType() == AccountType.EXPENSE
				|| trans.getAccount().getAccountType() == AccountType.INCOME)
				category = setString(trans.getAccount()==null?"":trans.getAccount().getAccountName());
			else
				transAcct = setString(trans.getAccount()==null?"":trans.getAccount().getAccountName());
			splitValue = setMoney(trans.getValue());
			parentValue = setMoney(pTxn.getValue());
			memo = pTxn.getMemo();
			splitType = setString(trans.getParameter("invest.splittype", ""));
		}
		if (invest.hasFee) {
			fee = setMoney(invest.fee);
			feeAcct =setString(invest.feeAcct==null?"":invest.feeAcct.getAccountName());
		}
		else {
			fee = 0L;
			feeAcct="";
		}
		if (invest.hasPrice) 
			price = setDouble(invest.price);
		else
			price = 0.0;
		if (invest.hasSecurity) 
			security = setString(invest.security==null?"":invest.security.getAccountName());
		else
			security = "";
		if (invest.hasShares)
			numShares = setLong(invest.shares);
		else
			numShares = 0L;
		parTxnId = trans.getParentTxn().getUUID();
		txnId= trans.getUUID();
		entered =setDate(Utilities.getSQLDate(trans.getDateInt()));
		datePosted = setDate(Utilities.getSQLDate(trans.getDatePostedOnline()));
		description = setString(trans.getDescription());
		status = setString(trans.getClearedStatus().toString());
		tax = setDate(Utilities.getSQLDate(trans.getTaxDateInt()));
		transfer = setString(trans.getTransferType());
		List<String> tagList = trans.getKeywords();
		String tagString = "";
		if (tagList != null && !tagList.isEmpty())
			for (String tag : tagList)
				tagString += tag;
		tags= setString(tagString);
		
	}
}
