package com.moneydance.modules.features.reportwriter.databeans;

import com.infinitekind.moneydance.model.CurrencyType;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class SecurityBean extends DataBean {
	@ColumnName("SecurityId")
	@ColumnTitle("Security ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String securityId;
	@ColumnName("SecurityName")
	@ColumnTitle("Security Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String securityName;
	@ColumnName("Ticker")
	@ColumnTitle("Ticker")
	@FieldType(BEANFIELDTYPE.STRING)
	public String ticker;
	@ColumnName("Prefix")
	@ColumnTitle("Prefix")
	@FieldType(BEANFIELDTYPE.STRING)
	public String prefix;
	@ColumnName("Suffix")
	@ColumnTitle("Suffix")
	@FieldType(BEANFIELDTYPE.STRING)
	public String suffix;
	@ColumnName("DecimalPlaces")
	@ColumnTitle("Decimal Places")
	@FieldType(BEANFIELDTYPE.INTEGER)
	public int numDecimal;
	private CurrencyType security;
	public SecurityBean() {
		super();
		tableName = "Security";
		shortName = "sec";
		parmsName = Constants.PARMFLDSEC;
	}
	public void setSecurity(CurrencyType security) {
		this.security=security;
	}
	@Override
	public void populateData() {
		securityId = setString(security.getIDString());
		securityName = setString(security.getName());
		ticker = setString(security.getTickerSymbol());
		prefix = setString(security.getPrefix());
		suffix = setString(security.getSuffix());
		numDecimal = setInt(security.getDecimalPlaces());

	}

}
