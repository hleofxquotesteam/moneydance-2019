package com.moneydance.modules.features.reportwriter.databeans;

import com.infinitekind.moneydance.model.CurrencyType;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class CurrencyBean extends DataBean {
	@ColumnName("CurrencyId")
	@ColumnTitle("Currency ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String currencyId;
	@ColumnName("CurrencyName")
	@ColumnTitle("Currency Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String currencyName;
	@ColumnName("Prefix")
	@ColumnTitle("Prefix")
	@FieldType(BEANFIELDTYPE.STRING)
	public String prefix;
	@ColumnName("Suffix")
	@ColumnTitle("Suffix")
	@FieldType(BEANFIELDTYPE.STRING)
	public String suffix;
	@ColumnName("DecimalPlaces")
	@ColumnTitle("Decimal Places")
	@FieldType(BEANFIELDTYPE.INTEGER)
	public int numDecimal;
	private CurrencyType currency;
	public CurrencyBean() {
		super();
		tableName = "Currency";
		shortName = "cur";
		parmsName = Constants.PARMFLDCUR;
	
	}
	public void setCurrency(CurrencyType currency) {
		this.currency=currency;
	}
	@Override
	public void populateData() {
		currencyId = setString(currency.getIDString());
		currencyName = setString(currency.getName());
		prefix = setString(currency.getPrefix());
		suffix = setString(currency.getSuffix());
		numDecimal = setInt(currency.getDecimalPlaces());

	}
}
