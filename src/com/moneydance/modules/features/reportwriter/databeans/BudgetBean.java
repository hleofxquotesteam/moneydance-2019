package com.moneydance.modules.features.reportwriter.databeans;

import com.infinitekind.moneydance.model.Budget;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class BudgetBean extends DataBean {
	@ColumnName("BudgetName")
	@ColumnTitle("Budget Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String name;
	@ColumnName("BudgetPeriod")
	@ColumnTitle("Budget Period")
	@FieldType(BEANFIELDTYPE.STRING)
	public String period;
	@ColumnName("isNewStyle")
	@ColumnTitle("Is New Style")
	@FieldType(BEANFIELDTYPE.BOOLEAN)
	public boolean isNewStyle;
	@ColumnName("BudgetId")
	@ColumnTitle("Budgtet ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String budgetId;

	
	/*
	 * Transient fields
	 */
	private transient Budget budget;
	
	public BudgetBean() {
		super();
		tableName = "Budget";
		shortName = "budg";
		parmsName = Constants.PARMFLDBUDG;
	}
	public void setBudget(Budget budget) {
		this.budget = budget;
	}

	

	@Override
	public void populateData() {
		name=setString(budget.getName());
		period=setString(budget.getPeriodType().toString());
		isNewStyle=setBoolean(budget.isNewStyle());
		budgetId = setString(budget.getUUID());
	}

}
