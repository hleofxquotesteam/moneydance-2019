package com.moneydance.modules.features.reportwriter.databeans;

import com.infinitekind.moneydance.model.AddressBookEntry;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class AddressBean extends DataBean {
		@ColumnName("Id")
		@ColumnTitle ("ID")
		@FieldType(BEANFIELDTYPE.LONG)
		private long id;
		@ColumnName("Name")
		@ColumnTitle ("Name")
		@FieldType(BEANFIELDTYPE.STRING)
		private String name;
		@ColumnName("Street")
		@ColumnTitle ("Street")
		@FieldType(BEANFIELDTYPE.STRING)
        private String street;
		@ColumnName("Phone")
		@ColumnTitle ("Phone")
		@FieldType(BEANFIELDTYPE.STRING)
        private String phone;
		@ColumnName("Email")
		@ColumnTitle ("Email")
		@FieldType(BEANFIELDTYPE.STRING)
		private String email;
		@ColumnName("UId")
		@ColumnTitle ("UID")
		@FieldType(BEANFIELDTYPE.STRING)
		private String uid;
	/*
	 * transient fields
	 */
	private transient AddressBookEntry address;
	public AddressBean() {
		super();
		tableName = "Address";
		shortName = "addr";
		parmsName = Constants.PARMFLDADDR;

	}

	public void setAddress(AddressBookEntry address) {
		this.address=address;
	}
	@Override
	public void populateData() {
		name = setString(address.getName());
		street= setString(address.getAddressString());
		email = setString(address.getEmailAddress());
		id= setLong(address.getID());
		phone = setString(address.getPhoneNumber());
		uid = setString(address.getUUID());
	}
}
