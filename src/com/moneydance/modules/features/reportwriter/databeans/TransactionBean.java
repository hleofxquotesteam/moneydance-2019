package com.moneydance.modules.features.reportwriter.databeans;

import java.util.List;

import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account.AccountType;
import com.infinitekind.moneydance.model.ParentTxn;
import com.infinitekind.moneydance.model.SplitTxn;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.Utilities;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class TransactionBean extends DataBean {
	@ColumnName("TxnType")
	@ColumnTitle("Transaction Type")
	@FieldType(BEANFIELDTYPE.STRING)
	public String txnType;
	@ColumnName("ParentTxnID")
	@ColumnTitle("Parent Txn ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String parTxnId;
	@ColumnName("TxnID")
	@ColumnTitle("Txn ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String txnId;
	@ColumnName("AccountName")
	@ColumnTitle("Account Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String account;
	@ColumnName("CheckNum")
	@ColumnTitle("Check Number")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String check;
	@ColumnName("DateEntered")
	@ColumnTitle("Date Entered")
	@FieldType(BEANFIELDTYPE.DATEINT)
	 public java.sql.Date entered;
	@ColumnName("DatePosted")
	@ColumnTitle("Date Posted Online")
	@FieldType(BEANFIELDTYPE.DATEINT)
	 public java.sql.Date datePosted;
	@ColumnName("Description")
	@ColumnTitle("Description")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String description;
	@ColumnName("Status")
	@ColumnTitle("Status")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String status;
	@ColumnName("TaxDate")
	@ColumnTitle("Tax Date")
	@FieldType(BEANFIELDTYPE.DATEINT)
	 public java.sql.Date tax;
	@ColumnTitle("Parent Value")
	@ColumnName("PrntValue")
	@FieldType(BEANFIELDTYPE.MONEY)
	 public long parentValue;
	@ColumnTitle("Split Value")
	@ColumnName("SpltValue")
	@FieldType(BEANFIELDTYPE.MONEY)
	 public long splitValue;
	@ColumnName("TransferType")
	@ColumnTitle("Transfer Type")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String transfer;
	@ColumnName("Tags")
	@ColumnTitle("Tags")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String tags;
	@ColumnName("Memo")
	@ColumnTitle("Medmo")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String memo;
	@ColumnName("Category")
	@ColumnTitle("Category")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String category;
	@ColumnTitle("Transfer Account")
	@ColumnName("TransAcct")
	@FieldType(BEANFIELDTYPE.STRING)
	 public String transAcct;;

	
	AbstractTxn trans;
	
	public TransactionBean () {
		super();
		tableName = "Trans";
		shortName = "trans";
		parmsName = Constants.PARMFLDTRAN;
		
	}
	public AbstractTxn getTrans() {
		return trans;
	}
	public void setTrans(AbstractTxn trans) {
		this.trans = trans;
	}

	@Override
	public void populateData() {
		ParentTxn pTxn=trans.getParentTxn();
		if (trans instanceof ParentTxn) {
			txnType = "P";
			account = setString(trans.getAccount()==null?"":trans.getAccount().getAccountName());
			category = "";
			parentValue = setMoney(trans.getValue());
			splitValue = 0L;
			memo = ((ParentTxn)trans).getMemo();
		}
		else {
			txnType = "S";
			account = "";
			if (trans.getAccount().getAccountType() == AccountType.EXPENSE
				|| trans.getAccount().getAccountType() == AccountType.INCOME)
				category = setString(trans.getAccount()==null?"":trans.getAccount().getAccountName());
			else
				transAcct = setString(trans.getAccount()==null?"":trans.getAccount().getAccountName());
			splitValue = setMoney(trans.getValue());
			parentValue = setMoney(pTxn.getValue());
			memo = pTxn.getMemo();
		}
		parTxnId = trans.getParentTxn().getUUID();
		txnId= trans.getUUID();
		check= setString(trans.getCheckNumber());
		entered =setDate(Utilities.getSQLDate(trans.getDateInt()));
		datePosted = setDate(Utilities.getSQLDate(trans.getDatePostedOnline()));
		description = setString(trans.getDescription());
		status = setString(trans.getClearedStatus().toString());
		tax = setDate(Utilities.getSQLDate(trans.getTaxDateInt()));
		transfer = setString(trans.getTransferType());
		List<String> tagList = trans.getKeywords();
		String tagString = "";
		if (tagList != null && !tagList.isEmpty())
			for (String tag : tagList)
				tagString += tag;
		tags= setString(tagString);
		
	}
}
