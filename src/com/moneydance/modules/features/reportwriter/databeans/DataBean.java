package com.moneydance.modules.features.reportwriter.databeans;
import java.lang.reflect.Field;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;
import com.moneydance.modules.features.reportwriter.view.DataParameter;
import com.moneydance.modules.features.reportwriter.view.SelectionDataRow;
public abstract class DataBean {
	protected String tableName;
	protected String shortName;
	protected String parmsName;
	protected SelectionDataRow selection;
	protected SortedMap<String,DataParameter> selParams;
	protected DataParameter dataParams;
	protected List<String> selFields;
	private List<BEANFIELDTYPE> columnTypes;
	public void setSelection(SelectionDataRow selection) {
		this.selection = selection;
		if (selection != null)
			selParams = selection.getParameters();
		else
			selParams = new TreeMap<String,DataParameter>();
		dataParams = selParams.get(parmsName);
		if (dataParams == null)
			dataParams = new DataParameter();
		selFields = dataParams.getList();
		if (selFields.isEmpty()) 
			selFields = null;
	}
	public void populateData() {}
	public String createTable() {
		StringBuilder bld = new StringBuilder("Create table "+tableName+ " (");
		Field[] fields = this.getClass().getDeclaredFields();
		boolean firstentry=true;
		for (Field field : fields) {
			if (!field.isAnnotationPresent(ColumnName.class)) // not database field
				continue;
			ColumnName name = field.getAnnotation(ColumnName.class);
			if (firstentry)
				firstentry = false;
			else
				bld.append(",");
			bld.append(name.value());
			FieldType type = field.getAnnotation(FieldType.class);
			switch (type.value()) {
			case STRING :
				bld.append(" Varchar(255)");
				break;
			case BOOLEAN:
				bld.append(" Boolean");
				break;
			case DATEINT:
				bld.append(" Date");
				break;
			case INTEGER:
				bld.append(" SmallInt");
				break;
			case LONG:
				bld.append(" BigInt");
				break;
			case MONEY:
				bld.append(" Decimal(19,4)");
				break;
			case DOUBLE:
			case PERCENT:
				bld.append(" Double");
				break;				
			default:
				break;
			
			}
		}
		bld.append(")");
		return bld.toString();	
	}
	public String[] createHeaderArray() {
		
		List<String> columns = new ArrayList<String>();
		columnTypes = new ArrayList<BEANFIELDTYPE>();
		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {
			if (!field.isAnnotationPresent(ColumnName.class)) // not database field
				continue;
			ColumnName name = field.getAnnotation(ColumnName.class);
			if (selFields != null && !selFields.contains(name.value())) // fields selected and not present
				continue;
			columns.add(name.value());
			FieldType type = field.getAnnotation(FieldType.class);
			columnTypes.add((BEANFIELDTYPE)type.value());
		}
		String[] array = new String[columns.size()];
		return columns.toArray(array);	
	}
	public String[] createStringArray() {
		List<String> columns = new ArrayList<String>();
		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {
			if (!field.isAnnotationPresent(ColumnName.class)) // not database field
				continue;
			ColumnName name = field.getAnnotation(ColumnName.class);
			if (selFields != null && !selFields.contains(name.value())) // fields selected and not present
				continue;
			FieldType type = field.getAnnotation(FieldType.class);
			switch (type.value()) {
			case STRING :
				try {
					columns.add((String)field.get(this));
				}
				catch (IllegalAccessException e) {
					columns.add("Error");
				}
				break;
			case BOOLEAN:
				try {
					columns.add(String.valueOf(field.getBoolean(this)));
				}
				catch (IllegalAccessException e) {
					columns.add("False");
				}
				break;
			case DATEINT:
				try {
					columns.add(((Date)field.get(this)).toString());
				}
				catch (IllegalAccessException e) {
					columns.add("1971-01-01");
				}
				break;
			case INTEGER:
				try {
					columns.add(String.valueOf(field.getInt(this)));
				}
				catch (IllegalAccessException e) {
					columns.add("0");
				}
				break;
			case LONG:
				try {
					columns.add(String.valueOf(field.getLong(this)));
				}
				catch (IllegalAccessException e) {
					columns.add("0L");
				}
				break;
		case MONEY:
				try {
					Double amt = ((Long)field.getLong(this)).doubleValue()/100.0;
					columns.add(amt.toString());
				}
				catch (IllegalAccessException e) {
					columns.add("0.0");
				}
				break;
			case DOUBLE:
			case PERCENT:
				try {
					columns.add(String.valueOf(field.getDouble(this)));
				}
				catch (IllegalAccessException e) {
					columns.add("0");
				}
				break;
			default:
				break;
			
			}
		}
		String[] array = new String[columns.size()];
		return columns.toArray(array);	
	}
	public BEANFIELDTYPE[] getColumnTypes() {
		BEANFIELDTYPE [] types= new BEANFIELDTYPE[columnTypes.size()];
		return columnTypes.toArray(types);
	}
	public String createSQL() {
		StringBuilder bld = new StringBuilder("Insert into "+tableName+ " (");
		Field[] fields = this.getClass().getDeclaredFields();
		boolean firstentry=true;
		for (Field field : fields) {
			if (!field.isAnnotationPresent(ColumnName.class)) // not database field
				continue;
			ColumnName name = field.getAnnotation(ColumnName.class);
			if (selFields != null && !selFields.contains(name.value())) // fields selected and not present
				continue;
			if (firstentry)
				firstentry = false;
			else
				bld.append(",");
			bld.append(name.value());
		}
		firstentry = true;
		bld.append(") values(");
		for (Field field : fields) {
			if (!field.isAnnotationPresent(ColumnName.class)) // not database field
				continue;
			ColumnName name = field.getAnnotation(ColumnName.class);
			if (selFields != null && !selFields.contains(name.value())) // fields selected and not present
				continue;
			if (firstentry)
				firstentry = false;
			else
				bld.append(",");
			FieldType type = field.getAnnotation(FieldType.class);
			switch (type.value()) {
			case STRING :
				bld.append("'");
				try {
					String tempStr = (String)field.get(this);
					if (tempStr != null)
						tempStr = tempStr.replace("'", "''");
					else
						tempStr = "";
					bld.append(tempStr);
				}
				catch (IllegalAccessException e) {
					bld.append("Error");
				}
				bld.append("'");				
				break;
			case BOOLEAN:
				try {
					bld.append(field.getBoolean(this));
				}
				catch (IllegalAccessException e) {
					bld.append("False");
				}
				break;
			case DATEINT:
				bld.append("Date '");
				try {
					bld.append(((Date)field.get(this)).toString());
				}
				catch (IllegalAccessException e) {
					bld.append("1971-01-01");
				}
				bld.append("'");
				break;
			case INTEGER:
				try {
					bld.append(field.getInt(this));
				}
				catch (IllegalAccessException e) {
					bld.append("0");
				}
				break;
			case LONG:
				try {
					bld.append(field.getLong(this));
				}
				catch (IllegalAccessException e) {
					bld.append("0");
				}
				break;
			case MONEY:
				try {
					Double amt = ((Long)field.getLong(this)).doubleValue()/100.0;
					bld.append(amt.toString());
				}
				catch (IllegalAccessException e) {
					bld.append("0.0");
				}
				break;
			case DOUBLE:
			case PERCENT:
				try {
					bld.append(field.getDouble(this));
				}
				catch (IllegalAccessException e) {
					bld.append("0");
				}
				break;
			default:
				break;
			
			}

		}
		bld.append(")");
		return bld.toString();
	}
	public String setString(String value) {
		if (value == null)
			return "";
		else
			return value;
	}
	public Long setMoney(Long value) {
		if (value == null)
			return 0L;
		else
			return value;
	}
	public Double setDouble(Double value) {
		if (value == null)
			return 0.0;
		else
			return value;
	}
	public Boolean setBoolean(Boolean value) {
		if (value == null)
			return false;
		else
			return value;
	}
	public Integer setInt(Integer value) {
		if (value == null)
			return 0;
		else
			return value;
	}
	public Long setLong(Long value) {
		if (value == null)
			return 0L;
		else
			return value;
	}
	public Date setDate(Date value) {
		if (value == null) 
			return new Date(0L);
		else
			return value;
	}

	public String getTableName () {return tableName;}
	public String getShortTableName () {return shortName;}
		
}
