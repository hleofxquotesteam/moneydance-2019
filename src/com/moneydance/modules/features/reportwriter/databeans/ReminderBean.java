package com.moneydance.modules.features.reportwriter.databeans;

import com.infinitekind.moneydance.model.Reminder;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.Utilities;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class ReminderBean extends DataBean {
	@ColumnName("AutoCommitDays")
	@ColumnTitle("Auto Commit Days")
	@FieldType(BEANFIELDTYPE.INTEGER)
  public int autoCommitDays; //ok
	@ColumnName("Description")
	@ColumnTitle("Description")
	@FieldType(BEANFIELDTYPE.STRING)
  public String description; //ok
	@ColumnName("IntialDate")
	@ColumnTitle("Initial Date")
	@FieldType(BEANFIELDTYPE.DATEINT)
  public java.sql.Date initialDate; //ok
	@ColumnName("LastDate")
	@ColumnTitle("Last Date")
	@FieldType(BEANFIELDTYPE.DATEINT)
  public java.sql.Date lastDate; //ok
	@ColumnName("Memo")
	@ColumnTitle("Memo")
	@FieldType(BEANFIELDTYPE.STRING)
  public String memo; //ok
	@ColumnName("RateAdjustmentOption")
	@ColumnTitle("Rate Adjustment Option")
	@FieldType(BEANFIELDTYPE.STRING)
  public String rateAdjustment; //ok
	@ColumnName("ReminderType")
	@ColumnTitle("Reminder Type")
	@FieldType(BEANFIELDTYPE.STRING)
  public String reminderType; //ok
	@ColumnName("RepeatDaily")
	@ColumnTitle("Repeat Daily")
	@FieldType(BEANFIELDTYPE.INTEGER)
  public int repeatDaily; //ok
	@ColumnName("RepeatMonthly")
	@ColumnTitle("Repeat Monthly")
	@FieldType(BEANFIELDTYPE.STRING)
  public String repeatMonthly; //ok
	@ColumnName("RepeatWeekly")
	@ColumnTitle("Repeat Weekly")
	@FieldType(BEANFIELDTYPE.STRING)
  public String repeatWeekly; //ok
	@ColumnName("RepeatYearly")
	@ColumnTitle("Repeat Yearly")
	@FieldType(BEANFIELDTYPE.BOOLEAN)
  public boolean repeatYearly; //ok
	@ColumnName("RepeatMonthlyMod")
	@ColumnTitle("Repeat Monthly Mod")
	@FieldType(BEANFIELDTYPE.INTEGER)
  public int repeatMonthlyMod; //ok
	@ColumnName("RepeatWeeklyMod")
	@ColumnTitle("Repeat WeeklyMod")
	@FieldType(BEANFIELDTYPE.INTEGER)
  public int repeatWeeklyMod; //ok
	@ColumnName("IsLoanReminder")
	@ColumnTitle("Loan Reminder")
	@FieldType(BEANFIELDTYPE.BOOLEAN)
  public boolean isLoanReminder; //ok
	@ColumnName("OccursOnDate")
	@ColumnTitle("Occurs On Date")
	@FieldType(BEANFIELDTYPE.DATEINT)
   public java.sql.Date occursOnDate; //ok
	@ColumnName("id")
	@ColumnTitle("ID")
	@FieldType(BEANFIELDTYPE.STRING)
  public String id; //ok

	/*
	 * Transient fields
	 */
	private Reminder  reminder;
	
	public ReminderBean() {
		super();
		tableName = "Reminder";
		shortName = "rmdr";
		parmsName = Constants.PARMFLDREM;
	}
	public Reminder getReminder() {
		return reminder;
	}
	public void setReminder(Reminder reminder) {
		this.reminder = reminder;
	}
	
	@Override
	public void populateData() {
		autoCommitDays = setInt(reminder.getAutoCommitDays());
		description = setString(reminder.getDescription());
		initialDate=setDate(Utilities.getSQLDate(reminder.getInitialDateInt()));
		lastDate=setDate(Utilities.getSQLDate(reminder.getLastDateInt()));
		memo = setString(reminder.getMemo());
		rateAdjustment = setString(reminder.getRateAdjustmentOption().toString());
		reminderType = setString(reminder.getReminderType().toString());
		repeatDaily = setInt(reminder.getRepeatDaily());
		int[] monthlyDays = reminder.getRepeatMonthly();
		if (monthlyDays != null && monthlyDays.length > 0) {
			String tempStr = "";
			for (int i=0;i<monthlyDays.length;i++)
				tempStr += String.valueOf(monthlyDays[i]);
			repeatMonthly = setString(tempStr);
		}
		else repeatMonthly = "";
		repeatMonthlyMod = setInt(reminder.getRepeatMonthlyModifier());
		int[] weeklyDays = reminder.getRepeatWeeklyDays();
		if (weeklyDays != null && weeklyDays.length > 0) {
			String tempStr = "";
			for (int i=0;i<weeklyDays.length;i++)
				tempStr += String.valueOf(weeklyDays[i]);
			repeatWeekly = setString(tempStr);
		}
		else repeatWeekly = "";
		repeatWeeklyMod = setInt(reminder.getRepeatWeeklyModifier());
		isLoanReminder= setBoolean(reminder.isLoanReminder());
		occursOnDate = setDate(Utilities.getSQLDate(reminder.getInitialDateInt()));
		id= setString(reminder.getUUID());
	}
}
