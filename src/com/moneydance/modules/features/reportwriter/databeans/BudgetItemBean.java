package com.moneydance.modules.features.reportwriter.databeans;

import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.Budget;
import com.infinitekind.moneydance.model.BudgetItem;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.Utilities;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class BudgetItemBean extends DataBean {
	@ColumnName("BudgetName")
	@ColumnTitle("Budget Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String name;
	@ColumnName("BudgetId")
	@ColumnTitle("Budgtet ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String budgetId;
	@ColumnName("BudgetItemId")
	@ColumnTitle("Budgtet Item ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String budgetItemId;
	@ColumnTitle("Amount")
	@ColumnName("Amouont")
	@FieldType(BEANFIELDTYPE.MONEY)
	 public long amount;
	@ColumnName("CurrencyTypeID")
	@ColumnTitle("Currency ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String currencyTypeID; //ok
	@ColumnName("CurrencyTypeName")
	@ColumnTitle("Currency Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String currencyTypeName; //ok
	@ColumnName("AccountName")
	@ColumnTitle("Account Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String account;
	@ColumnName("IsIncome")
	@ColumnTitle("Is Income")
	@FieldType(BEANFIELDTYPE.BOOLEAN)
	public boolean isIncome;
	@ColumnName("IntervalType")
	@ColumnTitle("Interval Type")
	@FieldType(BEANFIELDTYPE.STRING)
    public String intervalType; // ok
	@ColumnName("IntervalStartDate")
	@ColumnTitle("Interval Start Date")
	@FieldType(BEANFIELDTYPE.DATEINT)
	 public java.sql.Date start;
	@ColumnName("IntervalEndDate")
	@ColumnTitle("Interval End Date")
	@FieldType(BEANFIELDTYPE.DATEINT)
	 public java.sql.Date end;

	/*
	 * Transient fields
	 */
	private Budget budget;
	private BudgetItem budgetItem;

	public BudgetItemBean() {
		super();
		tableName = "BudgetItem";
		shortName = "budgi";
		parmsName = Constants.PARMFLDBUDGI;

	}
	public void setBean(Budget budget, BudgetItem budgetItem) {
		this.budget = budget;
		this.budgetItem = budgetItem;
		
	}
	@Override
	public void populateData() {
		name = setString(budget.getName());
		budgetId = budget.getUUID();
		budgetItemId = budgetItem.getUUID();
		amount = setMoney(budgetItem.getAmount());
		currencyTypeID=setString(budgetItem.getCurrency().getIDString());
		currencyTypeName=setString(budgetItem.getCurrency()==null?"":budgetItem.getCurrency().getName());
		account = setString(budgetItem.getTransferAccount().getAccountName());
		isIncome = setBoolean(budgetItem.isIncome());
		intervalType = setString(Constants.intervaltypes.get(budgetItem.getInterval()));
		start = setDate(Utilities.getSQLDate(budgetItem.getIntervalStartDate()));
		end = setDate(Utilities.getSQLDate(budgetItem.getIntervalEndDate()));
	}
}
