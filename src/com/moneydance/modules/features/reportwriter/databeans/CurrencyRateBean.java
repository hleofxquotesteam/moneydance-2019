package com.moneydance.modules.features.reportwriter.databeans;

import com.infinitekind.moneydance.model.CurrencySnapshot;
import com.infinitekind.moneydance.model.CurrencyType;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.Utilities;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class CurrencyRateBean extends DataBean {
	@ColumnName("CurrencyId")
	@ColumnTitle("Currency ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String currencyId;
	@ColumnName("Date")
	@ColumnTitle("Date")
	@FieldType(BEANFIELDTYPE.DATEINT)
	public java.sql.Date dateInt;	
	@ColumnName("DailyHigh")
	@ColumnTitle("Daily High")
	@FieldType(BEANFIELDTYPE.DOUBLE)
	public double dailyHigh;
	@ColumnName("DailyLow")
	@ColumnTitle("Daily Low")
	@FieldType(BEANFIELDTYPE.DOUBLE)
	public double dailyLow;
	@ColumnName("Rate")
	@ColumnTitle("Rate")
	@FieldType(BEANFIELDTYPE.DOUBLE)
	public double rate;
	@ColumnName("DailyVolume")
	@ColumnTitle("Daily Volume")
	@FieldType(BEANFIELDTYPE.LONG)
	public long dailyVolume;
	private CurrencyType currency;
	private CurrencySnapshot snapshot;
	public CurrencyRateBean() {
		super();
		tableName = "CurrencyRate";
		shortName = "currate";
		parmsName = Constants.PARMFLDCURR;
	}
	public void setBean(CurrencyType currency, CurrencySnapshot snapshot) {
		this.currency = currency;
		this.snapshot = snapshot;
	}
	@Override
	public void populateData() {
		currencyId = setString(currency.getIDString());
		dateInt = setDate(Utilities.getSQLDate(snapshot.getDateInt()));
		dailyHigh = setDouble(snapshot.getDailyHigh());
		dailyLow = setDouble(snapshot.getDailyLow());
		rate = setDouble(snapshot.getRate());
		dailyVolume = setLong(snapshot.getDailyVolume());

	}
}
