package com.moneydance.modules.features.reportwriter.databeans;

import com.infinitekind.moneydance.model.Account;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.Utilities;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;


public class AccountBean extends DataBean{
	@ColumnName("AccountID")
	@ColumnTitle("Account ID")
	@FieldType(BEANFIELDTYPE.STRING)
  public String accountId; //ok
	@ColumnName("AccountName")
	@ColumnTitle("Account Name")
	@FieldType(BEANFIELDTYPE.STRING)
  public String accountName; //ok
	@ColumnName("AccountType")
	@ColumnTitle("Account Type")
	@FieldType(BEANFIELDTYPE.STRING)
  public String accountType; // ok
	@ColumnName("AccountDescription")
	@ColumnTitle("Description")
	@FieldType(BEANFIELDTYPE.STRING)
public String accountDescription; // ok
	@ColumnName("AnnualFee")
	@ColumnTitle("Annual Fee")
	@FieldType(BEANFIELDTYPE.MONEY)
	public long annualFee; // ok
	@ColumnName("Apr")
	@ColumnTitle("APR")
	@FieldType(BEANFIELDTYPE.PERCENT)
  public double apr; //ok
	@ColumnName("AprPercent")
	@ColumnTitle("APR Percent")
	@FieldType(BEANFIELDTYPE.PERCENT)
  public double aprPercent; // ok
	@ColumnName("BankAccountNumber")
	@ColumnTitle("Account Number")
	@FieldType(BEANFIELDTYPE.STRING)
  public String bankAccountNumber; // ok
	@ColumnName("BankName")
	@ColumnTitle("Name of Bank")
	@FieldType(BEANFIELDTYPE.STRING)
  public String bankName; // ok
	@ColumnName("Broker")
	@ColumnTitle("Name of Broker")
	@FieldType(BEANFIELDTYPE.STRING)
  public String broker; // ok
	@ColumnName("BrokerPhone")
	@ColumnTitle("Broker Phone Num")
	@FieldType(BEANFIELDTYPE.STRING)
public String brokerPhone; //ok
	@ColumnName("CardExpirationMonth")
	@ColumnTitle("Card Expire Month")
	@FieldType(BEANFIELDTYPE.INTEGER)
  public int cardExpirationMonth; //ok
	@ColumnName("CardExpirationYear")
	@ColumnTitle("Card Expire Year")
	@FieldType(BEANFIELDTYPE.INTEGER)
  public int cardExpirationYear;//ok
	@ColumnName("CardNumber")
	@ColumnTitle("Card Long Number")
	@FieldType(BEANFIELDTYPE.STRING)
public String cardNumber; //ok
	@ColumnName("CreditLimit")
	@ColumnTitle("Credit Limit")
	@FieldType(BEANFIELDTYPE.MONEY)
  public long creditLimit; //ok
	@ColumnName("CurrencyTypeID")
	@ColumnTitle("Currency ID")
	@FieldType(BEANFIELDTYPE.STRING)
  public String currencyTypeID; //ok
	@ColumnName("CurrencyTypeName")
	@ColumnTitle("Currency Name")
	@FieldType(BEANFIELDTYPE.STRING)
  public String currencyTypeName; //ok
	@ColumnName("DefaultCategory")
	@ColumnTitle("Default Category")
	@FieldType(BEANFIELDTYPE.STRING)
  public String defaultCategory; //ok
	@ColumnName("FullAccountName")
	@ColumnTitle("Long Account Name")
	@FieldType(BEANFIELDTYPE.STRING)
  public String fullAccountName; //ok
	@ColumnName("InitialBalance")
	@ColumnTitle("Initial Balance")
	@FieldType(BEANFIELDTYPE.MONEY)
  public long  initialBalance; // ok startbalance
	@ColumnName("Comments")
	@ColumnTitle("Comments")
	@FieldType(BEANFIELDTYPE.STRING)
  public String comments; // ok
	@ColumnName("RoutingNumber")
	@ColumnTitle("Bank Routing/Sort Code")
	@FieldType(BEANFIELDTYPE.STRING)
  public String routingNumber; //ok OFXBankID
	@ColumnName("InitialDebt")
	@ColumnTitle("Initial Debt")
	@FieldType(BEANFIELDTYPE.MONEY)
  public long initialDebt; // same as start balance
	@ColumnName("PaymentPlan")
	@ColumnTitle("Payment Plan")
	@FieldType(BEANFIELDTYPE.STRING)
  public String paymentPlan; 	// tag pmt_spec
	@ColumnName("ppExpires")
	@ColumnTitle("Payment Expiry Date")
	@FieldType(BEANFIELDTYPE.DATEINT)
public java.sql.Date ppExpires; // tag exp_month & exp_year
	@ColumnName("AprUntil")
	@ColumnTitle("Date APR changes")
	@FieldType(BEANFIELDTYPE.DATEINT)
  public java.sql.Date aprUntil; // tag apr_changes_dt
	@ColumnName("AprNewAmount")
	@ColumnTitle("New APR Percent")
	@FieldType(BEANFIELDTYPE.PERCENT)
  public double aprNewAmount; // tag apr_Perm_rt
	@ColumnName("principal")
	@ColumnTitle("Principal Amount")
	@FieldType(BEANFIELDTYPE.MONEY)
  public long principal;// tag init_principal
	@ColumnName("LoanPoints")
	@ColumnTitle("Loan Points")
	@FieldType(BEANFIELDTYPE.DOUBLE)
  public double loanPoints; //tag points
	@ColumnName("PaymentsPerYear")
	@ColumnTitle("Payments Per Year")
	@FieldType(BEANFIELDTYPE.INTEGER)
  public int paymentsPerYear; //tag pmts_per_year
	@ColumnName("NumPayments")
	@ColumnTitle("Total Num Payments")
	@FieldType(BEANFIELDTYPE.INTEGER)
  public int numPayments; //tag numPayments
	@ColumnName("InterestCategory")
	@ColumnTitle("Category for Interest")
	@FieldType(BEANFIELDTYPE.STRING)
  public String interestCategory; // tag interest_account
	@ColumnName("EscrowPayment")
	@ColumnTitle("Escrow Payment Amt")
	@FieldType(BEANFIELDTYPE.MONEY)
  public long escrowPayment; //ok
	@ColumnName("EscrowAccount")
	@ColumnTitle("Escrow Account Name")
	@FieldType(BEANFIELDTYPE.STRING)
  public String escrowAcct;//ok
	@ColumnName("SpecificPayment")
	@ColumnTitle("Specific Payment")
	@FieldType(BEANFIELDTYPE.MONEY)
  public long specificPayment; // tag monthly_pmt
	@ColumnName("CalcPayment")
	@ColumnTitle("Calculate or Specify Payment")
	@FieldType(BEANFIELDTYPE.BOOLEAN)
  public boolean calcPayment; // tag calc_pmt
	@ColumnName("StartDate")
	@ColumnTitle("Start Date")
	@FieldType(BEANFIELDTYPE.DATEINT)
  public java.sql.Date startDate;// ok creation date
	@ColumnName("ClearedBalance")
	@ColumnTitle("Cleared Balance")
	@FieldType(BEANFIELDTYPE.MONEY)
  public long clearedBalance; // ok
	@ColumnName("ConfirmedBalance")
	@ColumnTitle("Confirmed Balance")
	@FieldType(BEANFIELDTYPE.MONEY)
  public long confirmedBalance; //ok
	@ColumnName("CurrentBalance")
	@ColumnTitle("Current Balance")
	@FieldType(BEANFIELDTYPE.MONEY)
  public long currentBalance;  //ok
	/*
	 * Transient fields
	 */
	private Account account;
	

	public AccountBean () {
		super();
		tableName = "Account";
		shortName = "acct";
		parmsName = Constants.PARMFLDACCT;
	}
	

	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	

	@Override
	public void populateData() {
		accountId = setString(account.getUUID());
		accountName = setString(account.getAccountName());
		accountType=setString(account.getAccountType().name());
		accountDescription=setString(account.getAccountDescription());
		annualFee=setMoney(account.getAnnualFee());
		apr=setDouble(account.getAPR());
		aprPercent=setDouble(account.getAPRPercent());
		aprUntil=setDate(Utilities.getSQLDate(account.getRateChangeDate()));
		bankAccountNumber=setString(account.getBankAccountNumber());
		bankName=setString(account.getBankName());
		routingNumber = setString(account.getOFXBankID());
		broker=setString(account.getBroker());
		brokerPhone=setString(account.getBrokerPhone());
		cardExpirationMonth=setInt(account.getCardExpirationMonth());
		cardExpirationYear=setInt(account.getCardExpirationYear());
		cardNumber=setString(account.getCardNumber());
		currencyTypeID=setString(account.getCurrencyType().getIDString());
		currencyTypeName=setString(account.getCurrencyType()==null?"":account.getCurrencyType().getName());
		defaultCategory=setString(account.getDefaultCategory()==null?"":account.getDefaultCategory().getAccountName());
		fullAccountName=setString(account.getFullAccountName());
		clearedBalance=setMoney(account.getClearedBalance());
		comments=setString(account.getComment());
		confirmedBalance=setMoney(account.getConfirmedBalance());
		creditLimit=setMoney(account.getCreditLimit());
		currentBalance=setMoney(account.getCurrentBalance());
		escrowAcct=setString(account.getEscrowAccount()==null?"":account.getEscrowAccount().getAccountName());
		escrowPayment=setMoney(account.getEscrowPayment());
		initialBalance=setMoney(account.getStartBalance());
		paymentsPerYear=setInt(account.getPaymentsPerYear());
		interestCategory=setString(account.getInterestAccount()==null?"":account.getInterestAccount().getAccountName());
		numPayments=setInt(account.getNumPayments());
		initialDebt=setMoney(account.getInitialPrincipal());
		calcPayment=setBoolean(account.getCalcPmt());
		loanPoints=setDouble(account.getPoints());
		ppExpires=setDate(Utilities.getSQLDate(account.getRateChangeDate()));
		aprNewAmount=setDouble(account.getPermanentAPR());
		startDate=setDate(Utilities.getSQLDate(account.getCreationDateInt()));
		paymentPlan = setString(account.getDebtPaymentSpec().toString());
	}

	public Account retrieveAccount() {
		return account;
	}

}

