package com.moneydance.modules.features.reportwriter.databeans;

import com.infinitekind.moneydance.model.Account;
import com.moneydance.modules.features.reportwriter.Constants;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.BEANFIELDTYPE;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnName;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.ColumnTitle;
import com.moneydance.modules.features.reportwriter.databeans.BeanAnnotations.FieldType;

public class CategoryBean extends DataBean {
	@ColumnName("CategoryID")
	@ColumnTitle("Category ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String categoryId; // ok
	@ColumnName("CategoryName")
	@ColumnTitle("Category Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String categoryName; // ok
	@ColumnName("CategoryType")
	@ColumnTitle("Category Type")
	@FieldType(BEANFIELDTYPE.STRING)
	public String categoryType; // ok
	@ColumnName("CurrencyTypeID")
	@ColumnTitle("Currency ID")
	@FieldType(BEANFIELDTYPE.STRING)
	public String currencyTypeID; // ok
	@ColumnName("CurrencyTypeName")
	@ColumnTitle("Currency Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String currencyTypeName; // ok
	@ColumnName("FullName")
	@ColumnTitle("Long Name")
	@FieldType(BEANFIELDTYPE.STRING)
	public String fullName; // ok
	@ColumnName("TaxRelated")
	@ColumnTitle("Tax Related")
	@FieldType(BEANFIELDTYPE.BOOLEAN)
	public boolean taxRelated; // ok
	private Account category;

	public CategoryBean() {
		super();
		tableName = "Category";
		shortName = "catg";
		parmsName = Constants.PARMFLDCAT;
	}

	public void setCategory(Account category) {
		this.category = category;
	}

	@Override
	public void populateData() {
		categoryId = setString(category.getUUID());
		categoryName = setString(category.getAccountName());
		categoryType = setString(category.getAccountType().name());
		currencyTypeID = setString(category.getCurrencyType().getIDString());
		currencyTypeName = setString(category.getCurrencyType() == null ? "" : category.getCurrencyType().getName());
		fullName = setString(category.getFullAccountName());
		taxRelated = setBoolean(category.isTaxRelated());
	}

}
