package com.moneydance.modules.features.reportwriter;

import java.io.IOException;
import java.io.InputStream;

import javafx.scene.image.Image;

public class Images {
	public Image editImg=null;
	public Image deleteImg = null;
	public Image viewImg = null;
	public Image okImg = null;
	public Image cancelImg = null;
	public Image closeImg = null;
	public Image settingsImg = null;
	public Image helpImg = null;
	public Image addImg = null;
	public Images() {
		InputStream stream = getClass().getResourceAsStream(Constants.RESOURCES+"icons8-edit-file-30.png");
		if (stream != null)
			editImg = new Image(stream);
		try {
			stream.close();
		}
		catch (IOException e) {}
		InputStream streamDel = getClass().getResourceAsStream(Constants.RESOURCES+"icons8-trash-can-30.png");
		if (streamDel != null)
			deleteImg = new Image(streamDel);
		try {
			streamDel.close();
		}
		catch (IOException e) {}
		InputStream streamView = getClass().getResourceAsStream(Constants.RESOURCES+"icons8-send-to-printer-30.png");
		if (streamView != null)
			viewImg = new Image(streamView);
		try {
			streamView.close();
		}
		catch (IOException e) {}
		InputStream streamOk = getClass().getResourceAsStream(Constants.RESOURCES+"001-checked.png");
		if (streamOk != null)
			okImg = new Image(streamOk);
		try {
			streamOk.close();
		}
		catch (IOException e) {}
		InputStream streamCancel = getClass().getResourceAsStream(Constants.RESOURCES+"002-cross.png");
		if (streamCancel != null)
			cancelImg = new Image(streamCancel);
		try {
			streamCancel.close();
		}
		catch (IOException e) {}
		InputStream streamClose = getClass().getResourceAsStream(Constants.RESOURCES+"icons8-exit-32.png");
		if (streamClose != null)
			closeImg = new Image(streamClose);
		try {
			streamClose.close();
		}
		catch (IOException e) {}
		InputStream streamSettings = getClass().getResourceAsStream(Constants.RESOURCES+"icons8-gear-30.png");
		if (streamSettings!= null)
			settingsImg = new Image(streamSettings);
		try {
			streamSettings.close();
		}
		catch (IOException e) {}
		InputStream streamAdd = getClass().getResourceAsStream(Constants.RESOURCES+"icons8-add-new-32.png");
		if (streamAdd!= null)
			addImg = new Image(streamAdd);
		try {
			streamAdd.close();
		}
		catch (IOException e) {}
		InputStream streamHelp = getClass().getResourceAsStream(Constants.RESOURCES+"icons8-help-32.png");
		if (streamHelp!= null)
			helpImg = new Image(streamHelp);
		try {
			streamHelp.close();
		}
		catch (IOException e) {}


	}

}
