/*
 * Copyright (c) 2020, Michael Bray.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
package com.moneydance.modules.features.reportwriter;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.ByteArrayOutputStream;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.moneydance.modules.features.mrbutil.MRBDebug;
import com.moneydance.modules.features.reportwriter.view.DataPane;
import com.moneydance.modules.features.reportwriter.view.ReportPane;
import com.moneydance.modules.features.reportwriter.view.SelectionPane;
import com.moneydance.modules.features.reportwriter.view.TemplatePane;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;


public class MyReport extends JFXPanel implements EventHandler<ActionEvent>{
	private Parameters params;
	/*
	 * Screen variables
	 */
	private Scene scene;
//	private VBox menuBox;
	private GridPane mainScreen;
//	private MainMenu menuBar;
	private TemplatePane templatePan;
	private SelectionPane selectionPan;
	private DataPane dataPan;
	private ReportPane reportPan;
	private MyReport thisObj;
	private Button closeBtn;
	private Button settingsBtn;
	private Button helpBtn;
	private HBox buttons;
	public int iFRAMEWIDTH;
	public int iFRAMEDEPTH;
	public int SCREENWIDTH=0;
	public int SCREENHEIGHT=0;
	public int BOXHEIGHT=0;

	public MyReport(){
		thisObj = this;
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (IllegalAccessException | UnsupportedLookAndFeelException | InstantiationException | ClassNotFoundException e) {}

	}
	public Scene createScene(){
		mainScreen = new GridPane();
		scene = new Scene(mainScreen);
		params = new Parameters();
		if (params.getDataDirectory()== null  || params.getDataDirectory().equals(Constants.NODIRECTORY)) {
			FirstRun setParams = new FirstRun(this,params);
			if (params.getDataDirectory().equals(Constants.NODIRECTORY)) {
				Alert alert = new Alert(AlertType.ERROR,"Extension can not continue without setting the directories");
				alert.showAndWait();
			}
			else
				params.save();
		}
		this.addComponentListener(new ComponentListener() {
			@Override
			public void componentResized(ComponentEvent e) {
				Main.rwDebugInst.debugThread("MyReport", "createScene", MRBDebug.SUMMARY, "Component New size "+thisObj.getWidth()+"/"+thisObj.getHeight());
				Main.preferences.put(Constants.PROGRAMNAME+"."+Constants.CRNTFRAMEWIDTH,thisObj.getWidth());
				Main.preferences.put(Constants.PROGRAMNAME+"."+Constants.CRNTFRAMEHEIGHT,thisObj.getHeight());
				updatePreferences(thisObj.getWidth(),thisObj.getHeight());
			}
	
			@Override
			public void componentMoved(ComponentEvent e) {
			}
	
			@Override
			public void componentShown(ComponentEvent e) {
			
			}
	
			@Override
			public void componentHidden(ComponentEvent e) {			
			}
		});

		closeBtn = new Button();
		if (Main.loadedIcons.closeImg == null)
			closeBtn.setText("Exit");
		else
			closeBtn.setGraphic(new ImageView(Main.loadedIcons.closeImg));
		closeBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						Main.extension.closeConsole();
					}
				});
			}
		});
		settingsBtn = new Button();
		if (Main.loadedIcons.settingsImg == null)
			settingsBtn.setText("Settings");
		else
			settingsBtn.setGraphic(new ImageView(Main.loadedIcons.settingsImg));
		settingsBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
			       Platform.runLater(new Runnable() {
			            @Override 
			            public void run() {
							FirstRun setParams = new FirstRun(thisObj,params);
							if (params.getDataDirectory().equals(Constants.NODIRECTORY)) {
								Alert alert = new Alert(AlertType.ERROR,"Extension can not continue without setting the directories");
								alert.showAndWait();
							}
							else
								params.save();			
						}
				});
			}
		});
		helpBtn = new Button();
		if (Main.loadedIcons.helpImg == null)
			helpBtn.setText("Help");
		else
			helpBtn.setGraphic(new ImageView(Main.loadedIcons.helpImg));
		helpBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
					}
				});
			}
		});
		templatePan = new TemplatePane(params);
		dataPan = new DataPane(params);
		reportPan = new ReportPane(params);
		selectionPan = new SelectionPane(params);
		mainScreen.add(templatePan, 0, 0);
		GridPane.setMargin(templatePan,new Insets(10,10,10,10));
		mainScreen.add(selectionPan, 1, 0);
		GridPane.setMargin(selectionPan,new Insets(10,10,10,10));
		mainScreen.add(dataPan, 0, 1);
		GridPane.setMargin(dataPan,new Insets(10,10,10,10));
		mainScreen.add(reportPan, 1, 1);
		GridPane.setMargin(reportPan,new Insets(10,10,10,10));
		buttons = new HBox();
		buttons.getChildren().addAll(closeBtn,settingsBtn,helpBtn);
		buttons.setSpacing(10);
		mainScreen.add(buttons, 0, 2);
		GridPane.setMargin(buttons,new Insets(10,10,10,10));
		String css="";
		try {
			java.io.InputStream in = 
					Main.loader.getResourceAsStream(Constants.RESOURCES+"datadatapane.css");
			if (in != null) {
				ByteArrayOutputStream bout = new ByteArrayOutputStream(1000);
				byte buf[] = new byte[256];
				int n = 0;
				while((n=in.read(buf, 0, buf.length))>=0)
					bout.write(buf, 0, n);
				css = bout.toString();
			}
		} catch (Throwable e) {
			e.printStackTrace();
			Main.rwDebugInst.debugThread("MyReport", "createScene", MRBDebug.SUMMARY, "css not found "+e.getLocalizedMessage());
			return scene;
		}
		Main.rwDebugInst.debugThread("MyReport", "createScene", MRBDebug.SUMMARY, "css "+css);
		mainScreen.setStyle(css);
		return scene;
	}



	@Override
	public void handle(ActionEvent event) {
		if (event.getSource() instanceof MenuItem) {
			MenuItem mItem = (MenuItem) event.getSource();
			String command = mItem.getText();
			switch (command) {
			case Constants.ITEMFILECLOSE :
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						Main.extension.closeConsole();
					}
				});
				break;
			case Constants.ITEMFILESAVE :
				params.save();
				break;
			case Constants.ITEMFILEOPTIONS :
				FirstRun setParams = new FirstRun(this,params);
				if (params.getDataDirectory().equals(Constants.NODIRECTORY)) {
					Alert alert = new Alert(AlertType.ERROR,"Extension can not continue without setting the directories");
					alert.showAndWait();
				}
				else
					params.save();			
			}
		}

	}
	public void setSizes() {
		setPreferences();
		this.setPreferredSize(new Dimension(iFRAMEWIDTH,iFRAMEDEPTH));
		if (mainScreen != null ) {
			SCREENWIDTH = (int) Math.round(mainScreen.getWidth());
			SCREENHEIGHT = (int) Math.round(mainScreen.getHeight());
			BOXHEIGHT = (int) Math.round(mainScreen.getHeight());
		}
		Main.rwDebugInst.debugThread("MyReport", "setSizes", MRBDebug.SUMMARY, "New size "+"/"+BOXHEIGHT+"/"+SCREENHEIGHT+"/"+SCREENWIDTH);
	}
	/*
	 * preferences
	 */
	private void setPreferences() {
		iFRAMEWIDTH = Main.preferences.getInt(Constants.PROGRAMNAME+"."+Constants.CRNTFRAMEWIDTH,Constants.MAINSCREENWIDTH);
		iFRAMEDEPTH = Main.preferences.getInt(Constants.PROGRAMNAME+"."+Constants.CRNTFRAMEHEIGHT,Constants.MAINSCREENHEIGHT);
	}

	private void updatePreferences(int width, int height) {
		SCREENHEIGHT = height;
		SCREENWIDTH = width;
		int dataWidth = (width)/2;
		int dataHeight = (height)/2;
		if (dataWidth<Constants.DATASCREENWIDTHMIN)
			dataWidth = Constants.DATASCREENWIDTHMIN;
		else
			if (dataWidth>Constants.DATASCREENWIDTHMAX)
				dataWidth = Constants.DATASCREENWIDTHMAX;
		if (dataHeight<Constants.DATASCREENHEIGHTMIN)
			dataHeight = Constants.DATASCREENHEIGHTMIN;
		else
			if (dataHeight>Constants.DATASCREENHEIGHTMAX)
				dataHeight = Constants.DATASCREENHEIGHTMAX;				
		Main.preferences.put(Constants.PROGRAMNAME+"."+Constants.DATAPANEWIDTH,dataWidth);
		Main.preferences.put(Constants.PROGRAMNAME+"."+Constants.DATAPANEHEIGHT,dataHeight);
		Main.preferences.isDirty();
		if (selectionPan != null)
			selectionPan.resize();
		if (reportPan != null)
			reportPan.resize();
		if (templatePan != null)
			templatePan.resize();
		if (dataPan != null)
			dataPan.resize();
		Main.rwDebugInst.debugThread("MyReport", "updatePreferences", MRBDebug.SUMMARY, "New size "+width+"/"+height+"/"+dataWidth+"/"+dataHeight);
	}
	public void resetData() {
		if (dataPan != null)
			dataPan.resetData();
		if (selectionPan != null)
			selectionPan.resetData();
		if (templatePan != null)
			templatePan.resetData();
		if (reportPan != null)
			reportPan.resetData();
	}

}
